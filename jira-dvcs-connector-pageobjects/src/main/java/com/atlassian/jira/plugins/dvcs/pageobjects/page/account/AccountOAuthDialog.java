package com.atlassian.jira.plugins.dvcs.pageobjects.page.account;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.WebDriverElement;
import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Represents dialog, which is fired on {@link Account#regenerate()}.
 */
public class AccountOAuthDialog extends WebDriverElement {
    @ElementBy(cssSelector = "div#tokenUser a")
    private PageElement authorizingAccountLink;

    @ElementBy(id = "currentKey")
    private PageElement currentKey;

    @ElementBy(cssSelector = "button.edit-button")
    private PageElement editButton;

    @ElementBy(xpath = ".//input[@name='key']")
    private PageElement keyField;

    @ElementBy(xpath = ".//input[@name='secret']")
    private PageElement secretField;

    @ElementBy(xpath = ".//button[contains(concat(' ', @class, ' '), ' submit ')]")
    private PageElement submitButton;

    public AccountOAuthDialog(By locator) {
        super(locator);
    }

    /**
     * @return OAuth key of this dialog.
     */
    public String getKey() {
        return currentKey.getText();
    }

    /**
     * @return OAuth secret of this dialog.
     */
    public String getSecret() {
        return secretField.getValue();
    }

    /**
     * Regenerates OAuth with provided key/secret.
     *
     * @param key    of OAuth
     * @param secret of OAuth
     */
    public void regenerate(String key, String secret) {
        edit();
        keyField.clear().type(key);
        secretField.clear().type(secret);
        submitButton.click();

        try {
            new WebDriverWait(driver, 30).until((Function<? super WebDriver, Boolean>) driver -> !this.isPresent());
        } catch (StaleElementReferenceException e) {
            // silently ignored - dialog will disappeared, we know about them
        }
    }

    public String getAuthorizingAccountAccountUrl() {
        return authorizingAccountLink.getAttribute("href");
    }

    public void edit() {
        editButton.click();
        waitUntilFalse(editButton.timed().isVisible());
        waitUntilTrue(keyField.timed().isVisible());
    }
}
