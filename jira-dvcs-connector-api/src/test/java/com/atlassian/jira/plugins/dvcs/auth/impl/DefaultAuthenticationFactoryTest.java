package com.atlassian.jira.plugins.dvcs.auth.impl;

import com.atlassian.jira.plugins.dvcs.auth.Authentication;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * @author Martin Skurla
 */
public final class DefaultAuthenticationFactoryTest {

    @Mock
    private Repository repositoryMock;

    private DefaultAuthenticationFactory classUnderTest;

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);

        classUnderTest = new DefaultAuthenticationFactory();
    }

    @Test
    public void testRepositoryWithNon3LOCredentialReturnsAnonymous() {
        when(repositoryMock.getCredential()).thenReturn(CredentialFactory.create2LOCredential("key", "secret"));

        Authentication authentication = classUnderTest.getAuthentication(repositoryMock);
        assertThat(authentication).isEqualTo(Authentication.ANONYMOUS);
    }

    @Test
    public void testRepositoryWith3LOCredentialShouldReturnOAuthAuthentication() {
        when(repositoryMock.getCredential()).thenReturn(CredentialFactory.create3LOCredential("key", "secret", "token"));

        Authentication authentication = classUnderTest.getAuthentication(repositoryMock);
        assertThat(authentication).isInstanceOf(OAuthAuthentication.class);
    }
}
