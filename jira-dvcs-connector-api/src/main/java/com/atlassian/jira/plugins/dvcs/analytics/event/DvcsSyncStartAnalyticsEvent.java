package com.atlassian.jira.plugins.dvcs.analytics.event;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Set;

import static com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag.SOFT_SYNC;
import static com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag.SYNC_CHANGESETS;
import static com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag.SYNC_PULL_REQUESTS;
import static com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag.WEBHOOK_SYNC;
import static com.google.common.base.Preconditions.checkNotNull;

@EventName("jira.dvcsconnector.sync.start")
public class DvcsSyncStartAnalyticsEvent {
    private final int syncId;
    private final int repoId;
    private final int organizationId;
    private final AuthType authType;
    private final String dvcsType;

    private final boolean soft;
    private final boolean commits;
    private final boolean pullrequests;
    private final boolean webhook;

    @ParametersAreNonnullByDefault
    public DvcsSyncStartAnalyticsEvent(final int syncId, final Repository repo, final Set<SynchronizationFlag> flags) {
        checkNotNull(repo);
        checkNotNull(flags);

        this.syncId = syncId;
        this.repoId = repo.getId();
        this.organizationId = repo.getOrganizationId();
        this.authType = AuthType.fromCredential(repo.getCredential());
        this.dvcsType = repo.getDvcsType();

        this.soft = flags.contains(SOFT_SYNC);
        this.commits = flags.contains(SYNC_CHANGESETS);
        this.pullrequests = flags.contains(SYNC_PULL_REQUESTS);
        this.webhook = flags.contains(WEBHOOK_SYNC);
    }

    public int getSyncId() {
        return syncId;
    }

    public int getRepoId() {
        return repoId;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public AuthType getAuthType() {
        return authType;
    }

    public String getDvcsType() {
        return dvcsType;
    }

    public boolean isSoft() {
        return soft;
    }

    public boolean isCommits() {
        return commits;
    }

    public boolean isPullrequests() {
        return pullrequests;
    }

    public boolean isWebhook() {
        return webhook;
    }
}
