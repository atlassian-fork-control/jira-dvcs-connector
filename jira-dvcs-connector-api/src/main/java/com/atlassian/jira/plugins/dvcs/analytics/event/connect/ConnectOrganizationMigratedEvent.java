package com.atlassian.jira.plugins.dvcs.analytics.event.connect;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.jira.plugins.dvcs.model.Organization;

import javax.annotation.Nonnull;

/**
 * Event fired when an {@link Organization} is migrated from a legacy authentication mechanism
 * (OAuth, basic etc.) to ACI JWT-based authentication.
 */
@EventName("jira.dvcsconnector.connect.organization.migrated")
public class ConnectOrganizationMigratedEvent extends ConnectOrganizationBaseEvent {
    public ConnectOrganizationMigratedEvent(@Nonnull final Organization org) {
        super(org);
    }
}
