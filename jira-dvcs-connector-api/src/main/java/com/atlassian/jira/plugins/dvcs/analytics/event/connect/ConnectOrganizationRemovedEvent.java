package com.atlassian.jira.plugins.dvcs.analytics.event.connect;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.jira.plugins.dvcs.model.Organization;

import javax.annotation.Nonnull;

/**
 * Event fired when an {@link com.atlassian.jira.plugins.dvcs.model.Organization} is removed via ACI
 */
@EventName("jira.dvcsconnector.connect.organization.removed")
public class ConnectOrganizationRemovedEvent extends ConnectOrganizationBaseEvent {
    public ConnectOrganizationRemovedEvent(@Nonnull final Organization org) {
        super(org);
    }
}
