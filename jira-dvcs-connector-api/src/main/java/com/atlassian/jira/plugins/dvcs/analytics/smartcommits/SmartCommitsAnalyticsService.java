package com.atlassian.jira.plugins.dvcs.analytics.smartcommits;

import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsType;

/**
 * A service to provide analytics for smart commits.
 */
public interface SmartCommitsAnalyticsService {
    /**
     * Fires an event to record that a DVCS organization has been added and if it has smart commits enabled.
     *
     * @param dvcsType            the kind of dvcs organization
     * @param smartCommitsEnabled whether smart commits are enable for the account
     */
    void fireNewOrganizationAddedWithSmartCommits(DvcsType dvcsType, boolean smartCommitsEnabled);

    /**
     * Fires an event to record a change in the smart commits enabled by default configuration for an organization
     *
     * @param orgId               the ID of the organization
     * @param smartCommitsEnabled the current state of the smart commit configuration
     */
    void fireSmartCommitAutoEnabledConfigChange(int orgId, boolean smartCommitsEnabled);

    /**
     * Fires an event to record a change to the smart commits configuration of an individual repository
     *
     * @param repoId              the repository ID
     * @param smartCommitsEnabled the current state of smart commits for that repository
     */
    void fireSmartCommitPerRepoConfigChange(int repoId, boolean smartCommitsEnabled);
}
