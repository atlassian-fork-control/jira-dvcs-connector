package com.atlassian.jira.plugins.dvcs.dao;

import com.atlassian.jira.plugins.dvcs.model.Repository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;
import java.util.List;

public interface RepositoryDao {
    /**
     * returns all repositories for given organization
     *
     * @param organizationId organizationId
     * @return repositories
     */
    List<Repository> getAllByOrganization(int organizationId, boolean includeDeleted);

    /**
     * Find the Repository with the given name in the provided organization, if it exists.
     *
     * @param organizationId The ID of the organization to find the repository in
     * @param repositoryName The name of the repository to find (unique for a given organization)
     * @return The repository, or <code>null</code> if none is found
     */
    Repository getByNameForOrganization(int organizationId, String repositoryName);

    /**
     * Gets all repositories across organizations.
     *
     * @param includeDeleted the also deleted
     * @return the all
     */
    List<Repository> getAll(boolean includeDeleted);

    boolean existsLinkedRepositories(boolean includeDeleted);

    /**
     * Returns the repository with the given ID.
     *
     * @param repositoryId repositoryId
     * @return <code>null</code> if not found
     */
    @Nullable
    Repository get(int repositoryId);

    /**
     * save Repository to storage. If it's new object (without ID) after this operation it will have it assigned.
     *
     * @param repository Repository
     * @return Repository
     */
    @Nonnull
    Repository save(@Nonnull Repository repository);

    /**
     * Deletes the repository with the given ID, plus any child rows in <code>RepositoryToProjectMapping</code>.
     *
     * @param repositoryId the ID of the repository to delete
     */
    void remove(int repositoryId);

    /**
     * Sets last pull request activity synchronization date
     *
     * @param repositoryId repository id
     * @param date         last synchronization date
     */
    void setLastActivitySyncDate(Integer repositoryId, Date date);

    List<Repository> getAllByType(String dvcsType, boolean includeDeleted);

    List<String> getPreviouslyLinkedProjects(int repositoryId);

    void setPreviouslyLinkedProjects(int forRepositoryId, Iterable<String> projectKeys);
}
