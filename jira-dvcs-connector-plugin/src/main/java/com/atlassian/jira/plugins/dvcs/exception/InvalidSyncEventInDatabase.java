package com.atlassian.jira.plugins.dvcs.exception;


public class InvalidSyncEventInDatabase extends RuntimeException {

    public InvalidSyncEventInDatabase(String message, Throwable cause) {
        super(message, cause);
    }

}
