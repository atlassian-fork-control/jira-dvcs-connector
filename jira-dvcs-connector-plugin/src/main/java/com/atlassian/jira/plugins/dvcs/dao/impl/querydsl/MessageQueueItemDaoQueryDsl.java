package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageQueueItemMapping;
import com.atlassian.jira.plugins.dvcs.dao.MessageQueueItemDao;
import com.atlassian.jira.plugins.dvcs.dao.impl.MessageQueueItemAoDaoImpl;
import com.atlassian.jira.plugins.dvcs.dao.impl.querydsl.util.PseudoStream;
import com.atlassian.jira.plugins.dvcs.dao.impl.transform.MessageEntityTransformer;
import com.atlassian.jira.plugins.dvcs.model.Message;
import com.atlassian.jira.plugins.dvcs.model.MessageId;
import com.atlassian.jira.plugins.dvcs.model.MessageQueueItem;
import com.atlassian.jira.plugins.dvcs.model.MessageState;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QMessageMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QMessageQueueItemMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QMessageTagMapping;
import com.atlassian.jira.plugins.dvcs.service.message.HasProgress;
import com.atlassian.jira.plugins.dvcs.service.message.MessageAddressService;
import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.google.common.annotations.VisibleForTesting;
import com.querydsl.core.Tuple;
import org.slf4j.Logger;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static com.atlassian.jira.plugins.dvcs.dao.impl.transform.TransformUtils.transformPayloadStringToClass;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;
import static org.slf4j.LoggerFactory.getLogger;

@Named("MessageQueueItemDaoQueryDsl")
@ParametersAreNonnullByDefault
public class MessageQueueItemDaoQueryDsl implements MessageQueueItemDao {
    private static final int PAGE_SIZE = 1000;
    private static final Logger log = getLogger(MessageQueueItemDaoQueryDsl.class);
    private final DatabaseAccessor databaseAccessor;
    private final MessageAddressService messageAddressService;
    private final MessageEntityTransformer messageEntityTransformer;
    private final MessageQueueItemAoDaoImpl messageQueueAoDao;

    @Inject
    public MessageQueueItemDaoQueryDsl(final DatabaseAccessor databaseAccessor,
                                       final MessageAddressService messageAddressService,
                                       final MessageEntityTransformer messageEntityTransformer,
                                       final MessageQueueItemAoDaoImpl messageQueueAoDAO) {
        this.databaseAccessor = checkNotNull(databaseAccessor);
        this.messageEntityTransformer = checkNotNull(messageEntityTransformer);
        this.messageQueueAoDao = checkNotNull(messageQueueAoDAO);
        this.messageAddressService = checkNotNull(messageAddressService);
    }

    @Override
    public MessageQueueItem create(final Map<String, Object> queueItem) {
        return toMessageQueueItem(messageQueueAoDao.create(queueItem));
    }

    @Override
    public void save(final MessageQueueItem messageQueueItem) {
        databaseAccessor.runInTransaction(connection -> {
            final QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();
            return connection.update(qMessageQueueItemMapping)
                    .set(qMessageQueueItemMapping.LAST_FAILED, messageQueueItem.getLastFailed())
                    .set(qMessageQueueItemMapping.MESSAGE_ID, messageQueueItem.getMessage().getId())
                    .set(qMessageQueueItemMapping.RETRIES_COUNT, messageQueueItem.getRetryCount())
                    .set(qMessageQueueItemMapping.STATE, messageQueueItem.getState())
                    .set(qMessageQueueItemMapping.STATE_INFO, messageQueueItem.getStateInfo())
                    .set(qMessageQueueItemMapping.QUEUE, messageQueueItem.getQueue())
                    .where(qMessageQueueItemMapping.ID.eq(messageQueueItem.getId()))
                    .execute();
        });
    }

    @Override
    public void delete(final MessageQueueItem messageQueueItem) {
        delete(messageQueueItem.getId());
    }

    @Override
    public void delete(final int messageQueueItemId) {
        databaseAccessor.runInTransaction(connection -> {
            QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();

            return connection.delete(qMessageQueueItemMapping)
                    .where(qMessageQueueItemMapping.ID.eq(messageQueueItemId))
                    .execute();
        });
    }

    @Override
    public MessageQueueItem[] getByMessageId(final MessageId messageId) {
        return Stream.of(messageQueueAoDao.getByMessageId(messageId.getId()))
                .map(this::toMessageQueueItem).toArray(MessageQueueItem[]::new);
    }

    @Override
    public Optional<MessageQueueItem> getQueueItemById(final int id) {
        List<Tuple> results = getRawQueueItemByQueueItemID(id);
        if (results.size() > 0) {
            Map<Integer, Message> messageMap = getMessagesFromResult(results);
            return Optional.of(toMessageQueueItem(results.get(0), messageMap));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<MessageQueueItem> getByQueueAndMessage(final String queue, final int messageId) {
        MessageQueueItemMapping messageQueueItemMapping = messageQueueAoDao.getByQueueAndMessage(queue, messageId);
        if (Objects.nonNull(messageQueueItemMapping)) {
            return Optional.of(toMessageQueueItem(messageQueueItemMapping));
        } else {
            return Optional.empty();

        }
    }

    @Override
    @Nullable
    public Message getNextItemForProcessing(final String queue, final String address) {
        final MessageQueueItemMapping messageQueueItem = messageQueueAoDao.getNextItemForProcessing(queue, address);
        if (messageQueueItem == null) {
            return null;
        }
        return messageEntityTransformer.toMessage(messageQueueItem.getMessage());
    }

    @Override
    public void getByTagAndState(final String tag, final MessageState state, final Consumer<Integer> consumer) {
        PseudoStream.consumeAllInTable(
                () -> getInitialIndexByTagAndState(tag, state),
                (index) -> getPageByTagAndState(tag, state, index),
                PseudoStream::getMax,
                identity(),
                consumer);
    }

    @Override
    public void getByState(final MessageState state, final Consumer<Integer> consumer) {
        PseudoStream.consumeAllInTable(
                () -> getInitialIndexByState(state),
                (index) -> getPageByState(state, index),
                PseudoStream::getMax,
                identity(),
                consumer);
    }

    private List<Integer> getPageByState(final MessageState state, final int startId) {
        return databaseAccessor.runInTransaction(connection -> {
            QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();
            final QMessageTagMapping qMessageTagMapping = new QMessageTagMapping();

            return connection.select(qMessageQueueItemMapping.ID).from(qMessageQueueItemMapping)
                    .join(qMessageTagMapping).on(qMessageQueueItemMapping.MESSAGE_ID.eq(qMessageTagMapping.MESSAGE_ID))
                    .where(qMessageQueueItemMapping.STATE.eq(state.name())
                            .and(qMessageQueueItemMapping.ID.gt(startId)))
                    .limit(PAGE_SIZE)
                    .orderBy(qMessageQueueItemMapping.ID.asc())
                    .fetch();
        });
    }

    /**
     * Gets the largest Id number that is smaller than the minimum ID in the result set
     * ie one smaller than the minimum id used
     *
     * @param state the state we are selecting on
     * @return the largest Id that is smaller than minimum ID value found
     */
    private Integer getInitialIndexByState(final MessageState state) {
        return databaseAccessor.runInTransaction(connection -> {
            QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();
            return connection.select(qMessageQueueItemMapping.ID.min())
                    .from(qMessageQueueItemMapping)
                    .where(qMessageQueueItemMapping.STATE.eq(state.name()))
                    .fetch()
                    .stream()
                    .map(Optional::ofNullable)
                    .findFirst().get()
                    .orElse(0) - 1;
        });
    }

    private List<Integer> getPageByTagAndState(final String tag, final MessageState state, final int startId) {
        return databaseAccessor.runInTransaction(connection -> {
                    final QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();
                    final QMessageTagMapping qMessageTagMapping = new QMessageTagMapping();
                    return connection.select(qMessageQueueItemMapping.ID).from(qMessageQueueItemMapping)
                            .join(qMessageTagMapping).on(qMessageQueueItemMapping.MESSAGE_ID.eq(qMessageTagMapping.MESSAGE_ID))
                            .where(qMessageQueueItemMapping.STATE.eq(state.name())
                                    .and(qMessageTagMapping.TAG.eq(tag))
                                    .and(qMessageQueueItemMapping.ID.gt(startId)))
                            .limit(PAGE_SIZE)
                            .orderBy(qMessageQueueItemMapping.ID.asc())
                            .fetch();
                }
        );
    }

    /**
     * Gets the largest Id number that is smaller than the minimum ID in the result set
     *
     * @param state the state we are selecting on
     * @return the largest Id that is smaller than minimum ID value found
     */
    private Integer getInitialIndexByTagAndState(final String tag, final MessageState state) {
        return databaseAccessor.runInTransaction(connection -> {
            final QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();
            final QMessageTagMapping qMessageTagMapping = new QMessageTagMapping();

            return connection.select(qMessageQueueItemMapping.ID.min()).from(qMessageQueueItemMapping)
                    .join(qMessageTagMapping).on(qMessageQueueItemMapping.MESSAGE_ID.eq(qMessageTagMapping.MESSAGE_ID))
                    .where(qMessageQueueItemMapping.STATE.eq(state.name())
                            .and(qMessageTagMapping.TAG.eq(tag)))
                    .fetch().stream()
                    .map(Optional::ofNullable)
                    .findFirst().get()
                    .orElse(0) - 1;
        });
    }

    private List<Tuple> getRawQueueItemByQueueItemID(final int queueItemId) {
        return databaseAccessor.runInTransaction(connection -> {
            final QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();
            final QMessageTagMapping qMessageTagMapping = new QMessageTagMapping();
            final QMessageMapping qMessageMapping = new QMessageMapping();

            return connection.select(qMessageQueueItemMapping.ID,
                    qMessageQueueItemMapping.LAST_FAILED,
                    qMessageQueueItemMapping.RETRIES_COUNT,
                    qMessageQueueItemMapping.STATE,
                    qMessageQueueItemMapping.STATE_INFO,
                    qMessageQueueItemMapping.QUEUE,
                    qMessageQueueItemMapping.MESSAGE_ID,
                    qMessageMapping.ID,
                    qMessageMapping.ADDRESS,
                    qMessageMapping.PAYLOAD,
                    qMessageMapping.PAYLOAD_TYPE,
                    qMessageMapping.PRIORITY,
                    qMessageTagMapping.ID,
                    qMessageTagMapping.TAG).from(qMessageQueueItemMapping)
                    .join(qMessageTagMapping).on(qMessageQueueItemMapping.MESSAGE_ID.eq(qMessageTagMapping.MESSAGE_ID))
                    .join(qMessageMapping).on(qMessageQueueItemMapping.MESSAGE_ID.eq(qMessageMapping.ID))
                    .where(qMessageQueueItemMapping.ID.eq(queueItemId))
                    .limit(PAGE_SIZE)
                    .orderBy(qMessageQueueItemMapping.ID.asc())
                    .fetch();
        });
    }

    @VisibleForTesting
    protected Map<Integer, Message> getMessagesFromResult(final List<Tuple> results) {
        final QMessageMapping qMessageMapping = new QMessageMapping();
        return results.stream()
                .collect(groupingBy(t -> t.get(qMessageMapping.ID)))
                .entrySet().stream()
                .collect(toMap(
                        Map.Entry::getKey,
                        entry -> flattenToMessage(entry.getValue())));

    }

    @VisibleForTesting
    protected MessageQueueItem toMessageQueueItem(final MessageQueueItemMapping mapping) {
        MessageQueueItem queueItem = new MessageQueueItem();
        queueItem.setId(mapping.getID());
        queueItem.setMessage(messageEntityTransformer.toMessage(mapping.getMessage()));
        queueItem.setRetryCount(mapping.getRetriesCount());
        queueItem.setLastFailed(mapping.getLastFailed());
        queueItem.setQueue(mapping.getQueue());
        queueItem.setState(mapping.getState());
        queueItem.setStateInfo(mapping.getStateInfo());
        return queueItem;
    }

    @VisibleForTesting
    protected MessageQueueItem toMessageQueueItem(final Tuple rawQueueResult, final Map<Integer, Message> messages) {
        QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();

        final Integer id = rawQueueResult.get(qMessageQueueItemMapping.ID);
        if (id == null) {
            throw new IllegalArgumentException("Null ID in database");
        }
        final Integer retries = rawQueueResult.get(qMessageQueueItemMapping.RETRIES_COUNT);
        if (retries == null) {
            throw new IllegalArgumentException("Null retires in database for message: " + id);
        }

        final MessageQueueItem queueItem = new MessageQueueItem();
        queueItem.setId(id);
        queueItem.setMessage(messages.get(rawQueueResult.get(qMessageQueueItemMapping.MESSAGE_ID)));
        queueItem.setRetryCount(retries);
        queueItem.setLastFailed(rawQueueResult.get(qMessageQueueItemMapping.LAST_FAILED));
        queueItem.setQueue(rawQueueResult.get(qMessageQueueItemMapping.QUEUE));
        queueItem.setState(rawQueueResult.get(qMessageQueueItemMapping.STATE));
        queueItem.setStateInfo(rawQueueResult.get(qMessageQueueItemMapping.STATE_INFO));
        return queueItem;
    }

    /**
     * Flattens a collection of result tuples with the same ID down to a single message.
     *
     * @param allResults the collection of results we want flattened
     * @param <P>        the type of payload the message contains
     * @return the message produced from aggregating the database result tuples
     */
    @SuppressWarnings("unchecked")
    private <P extends HasProgress> Message<P> flattenToMessage(final Collection<Tuple> allResults) {
        final QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();
        final QMessageMapping qMessageMapping = new QMessageMapping();

        final Tuple firstResult = allResults.stream().findFirst().get();
        final Class payloadType = transformPayloadStringToClass(firstResult.get(qMessageMapping.PAYLOAD_TYPE));
        final int retriesCount = allResults.stream()
                .map(t -> t.get(qMessageQueueItemMapping.RETRIES_COUNT))
                .reduce(Integer::max).orElse(0);

        final String address = firstResult.get(qMessageMapping.ADDRESS);
        if (address == null) {
            throw new IllegalArgumentException("address was null for message " + firstResult.get(qMessageMapping.ID));
        }
        final Integer priority = firstResult.get(qMessageMapping.PRIORITY);
        if (priority == null) {
            throw new IllegalArgumentException("priority was null for message " + firstResult.get(qMessageMapping.ID));
        }

        final Message<P> message = new Message<>();
        message.setId(firstResult.get(qMessageMapping.ID));
        message.setAddress(messageAddressService.get(payloadType, address));
        message.setPayload(firstResult.get(qMessageMapping.PAYLOAD));
        message.setPayloadType(payloadType);
        message.setPriority(priority);
        message.setTags(getAllTags(allResults));
        message.setRetriesCount(retriesCount);
        return message;
    }

    private String[] getAllTags(final Collection<Tuple> rawTags) {
        final QMessageTagMapping qMessageTagMapping = new QMessageTagMapping();
        return rawTags.stream().map(t -> t.get(qMessageTagMapping.TAG)).toArray(String[]::new);
    }
}
