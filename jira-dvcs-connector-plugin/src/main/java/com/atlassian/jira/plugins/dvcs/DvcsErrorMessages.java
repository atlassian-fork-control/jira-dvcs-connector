package com.atlassian.jira.plugins.dvcs;

public final class DvcsErrorMessages {

    public static final String DVCS_SYNC_PAUSED_KEY = "com.atlassian.jira.plugins.dvcs.sync.paused";

    public static final String GITHUB_RATE_LIMIT_REACHED_ERROR_KEY = "com.atlassian.jira.plugins.dvcs.github.rate.limit.reached";

    /**
     * The {@link com.atlassian.jira.help.HelpUrls} key for the generic sync error.
     */
    public static final String GENERIC_ERROR_HELP_URL_KEY = "link.dvcs.generic.sync.error";

    private DvcsErrorMessages() {}
}
