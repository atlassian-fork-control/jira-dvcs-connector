package com.atlassian.jira.plugins.dvcs.util;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;

import javax.annotation.Nullable;

/**
 * Utility methods relating to users.
 */
public final class UserUtils {
    private UserUtils() {
    }

    /**
     * Tries to convert the given object to a JIRA user.
     *
     * @param object the object to convert
     * @return null if a null object is given
     */
    @Nullable
    public static ApplicationUser from(@Nullable final Object object) {
        if (object == null) {
            return null;
        } else if (object instanceof ApplicationUser) {
            return (ApplicationUser) object;
        } else if (object instanceof User) {
            return ApplicationUsers.from((User) object);
        } else {
            throw new IllegalArgumentException("Unknown object type. Cannot convert to ApplicationUser: " + object);
        }
    }
}
