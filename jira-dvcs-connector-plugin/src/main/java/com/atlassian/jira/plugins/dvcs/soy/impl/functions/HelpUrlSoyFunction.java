package com.atlassian.jira.plugins.dvcs.soy.impl.functions;

import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.defaultString;

/**
 * Helper soy function to dynamically generate the help urls
 * Copy/paste from devstatus plugin
 */
@Scanned
public class HelpUrlSoyFunction implements SoyServerFunction, SoyClientFunction {

    private static final Set<Integer> ARGS_SIZES = ImmutableSet.of(1);
    private final HelpUrls helpUrls;
    private final Pattern SOY_STRING_PATTERN = Pattern.compile("^'(.*)'$");
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final Logger LOG = LoggerFactory.getLogger(HelpUrlSoyFunction.class);

    @Inject
    public HelpUrlSoyFunction(@ComponentImport HelpUrls helpUrls) {
        this.helpUrls = checkNotNull(helpUrls);
    }

    @Override
    public String getName() {
        // Note that the 'dvcs' prefix is used to avoid collisions with other soy functions
        // provided by other plugins, all of which exist in a global namespace.
        return "dvcs_getHelpUrl";
    }

    /**
     * @return a set containing the valid argument lengths which this function accepts.
     */
    @Override
    public Set<Integer> validArgSizes() {
        return ARGS_SIZES;
    }

    /**
     * Generates a JavasScript expression from the given JavaScript expressions.
     * Data will be used from HelpUrls
     */
    @Override
    public JsExpression generate(JsExpression... args) {
        if (args.length == 0) {
            throw new IllegalArgumentException("Help url requires at least one argument and it should" +
                    " be a string as the key to the help urls mapping.");
        }
        final Matcher match = SOY_STRING_PATTERN.matcher(args[0].getText());
        if (!match.matches()) {
            throw new IllegalArgumentException("Help url requires the argument to be a string as the key to the help urls mapping.");
        }

        final String helpUrlKey = match.group(1);
        final HelpUrl helpUrl = helpUrls.getUrl(helpUrlKey);
        String jsonExpr = null;
        final Map helpUrlMap = ImmutableMap.of(
                "alt", defaultString(helpUrl.getAlt()),
                "title", defaultString(helpUrl.getTitle()),
                "url", defaultString(helpUrl.getUrl()));
        try {
            jsonExpr = OBJECT_MAPPER.writeValueAsString(helpUrlMap);
        } catch (IOException e) {
            LOG.error("Error serialising to JSON: " + helpUrlMap, e);
        }

        return new JsExpression("(" + jsonExpr + ")");
    }

    /**
     * If soy is rendered at backend, we return the HelpUrl instance for it
     */
    @Override
    public Object apply(Object... args) {
        if (args.length == 0) {
            throw new IllegalArgumentException("Help url requires at least one argument and it should" +
                    " be a string as the key to the help urls mapping.");
        }

        if (!(args[0] instanceof String) || args[0] == null) {
            throw new IllegalArgumentException("Help url requires the argument to be a string as the key to the help urls mapping.");
        }
        return helpUrls.getUrl((String) args[0]);
    }
}
