package it.com.atlassian.jira.plugins.dvcs;

import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.rest.json.CredentialJson;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.httpclient.HttpClientError;

import javax.ws.rs.core.MediaType;
import java.util.List;

import static org.apache.http.HttpStatus.SC_OK;

/**
 * Can be used to invoke DVCS Connector REST endpoints.
 */
public class DvcsConnectorRestClient extends RestApiClient<DvcsConnectorRestClient> {
    private final String rootPath;

    public DvcsConnectorRestClient(final JIRAEnvironmentData environmentData) {
        super(environmentData);
        this.rootPath = environmentData.getBaseUrl().toExternalForm();
    }

    public Response<Repository> getRepository(int id) {
        return toResponse(() -> {
            return createRootResource()
                    .path("repository")
                    .path(Integer.toString(id))
                    .type(MediaType.APPLICATION_JSON)
                    .get(ClientResponse.class);
        }, Repository.class);
    }

    public Response<Organization> getOrganization(int id) {
        return toResponse(() -> {
            return createRootResource()
                    .path("organization")
                    .path(Integer.toString(id))
                    .type(MediaType.APPLICATION_JSON)
                    .get(ClientResponse.class);
        }, Organization.class);
    }

    public Response<Organization> postOrganization(Organization org) {
        return toResponse(() -> {
            return createRootResource()
                    .path("organization")
                    .type(MediaType.APPLICATION_JSON)
                    .post(ClientResponse.class, org);
        }, Organization.class);
    }

    /**
     * Deletes all organizations known to the DVCS Connector.
     *
     * @throws HttpClientError if there's a problem getting the existing organizations
     */
    public void deleteAllOrganizations() {
        final Response<List<Organization>> existingOrgs = getOrganizations();
        if (existingOrgs.statusCode != SC_OK) {
            throw new HttpClientError(String.format("Error code %d retrieving existing orgs", existingOrgs.statusCode));
        }
        existingOrgs.body.stream().forEach(org -> deleteOrganization(org.getId()));
    }

    public Response deleteOrganization(int orgId) {
        return deleteOrganization(orgId, false);
    }

    public Response deleteOrganization(int orgId, boolean allowIntegratedAccountRemoval) {
        return toResponse(() -> createRootResource()
                .path("organization")
                .path(Integer.toString(orgId))
                .queryParam("allowIntegratedAccountRemoval", Boolean.toString(allowIntegratedAccountRemoval))
                .type(MediaType.APPLICATION_JSON)
                .delete(ClientResponse.class, orgId));
    }

    public Response updateOrganizationCredential(final int id, final CredentialJson credentialRaw) {
        return toResponse(() -> {
            return createRootResource()
                    .path("organization")
                    .path(Integer.toString(id))
                    .path("credential")
                    .type(MediaType.APPLICATION_JSON)
                    .post(ClientResponse.class, credentialRaw);
        }, Response.class);
    }

    public Response syncRepoList(final int id) {
        return toResponse(() -> createRootResource()
                .path("organization")
                .path(Integer.toString(id))
                .path("syncRepoList")
                .type(MediaType.APPLICATION_JSON)
                .get(ClientResponse.class));
    }

    public Response enableRepository(final int id) {
        return toResponse(() -> createRootResource()
                .path("repository")
                .path(Integer.toString(id))
                .path("enable")
                .type(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class));
    }

    public Response disableRepository(final int id) {
        return toResponse(() -> createRootResource()
                .path("repository")
                .path(Integer.toString(id))
                .path("disable")
                .type(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class));
    }

    public Response startRepoFullSync(final int id) {
        return toResponse(() -> createRootResource()
                .path("repository")
                .path(Integer.toString(id))
                .path("fullsync")
                .type(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class));
    }

    public Response<List<Organization>> getOrganizations() {
        return toResponse(() -> {
            return createRootResource()
                    .path("organization")
                    .type(MediaType.APPLICATION_JSON)
                    .get(ClientResponse.class);
        }, new GenericType<List<Organization>>() {
        });
    }

    public Response pendAllOrganizations() {
        return toResponse(() -> createRootResource()
                .path("organization")
                .path("pending")
                .type(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class));
    }

    private WebResource createRootResource() {
        return resourceRoot(rootPath).path("rest").path("bitbucket").path("1.0");
    }
}
