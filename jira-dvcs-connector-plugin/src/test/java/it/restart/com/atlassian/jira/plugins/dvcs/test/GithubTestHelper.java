package it.restart.com.atlassian.jira.plugins.dvcs.test;

import com.atlassian.jira.plugins.dvcs.github.api.GitHubRESTClient;
import com.atlassian.jira.plugins.dvcs.github.api.model.GitHubRateLimit;
import com.atlassian.jira.plugins.dvcs.github.api.model.GitHubRepositoryHook;
import com.atlassian.jira.plugins.dvcs.github.impl.GitHubRESTClientImpl;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.atlassian.jira.plugins.dvcs.util.PasswordUtil;

import java.util.Collection;
import java.util.List;

import static com.atlassian.jira.plugins.dvcs.util.PasswordUtil.getPassword;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

/**
 * Some static helpers for the Github tests
 */
public final class GithubTestHelper {

    public static final String GITHUB_URL = "https://github.com";

    public static final String GITHUB_API_URL = "https://api.github.com";

    public static final String REPOSITORY_NAME = "test-project";

    public static List<String> getHookUrls(final String accountName, final String gitUrl, final String project) {
        final GitHubRESTClient restClient = new GitHubRESTClientImpl();
        final Repository repository = getRepository(accountName, gitUrl, project);
        final Collection<GitHubRepositoryHook> hooks =
                restClient.getHooks(repository, accountName, getPassword(accountName));
        return hooks.stream().map(hook -> hook.getConfig().get("url")).collect(toList());
    }

    private static Repository getRepository(final String accountName, final String gitUrl, final String project) {
        final Repository repository = new Repository();
        repository.setCredential(CredentialFactory.create3LOCredential("key", "secret", "bogus"));
        repository.setOrgHostUrl(gitUrl);
        repository.setOrgName(accountName);
        repository.setSlug(project);
        return repository;
    }

    /**
     * Returns the rate limit for authenticated requests made by the given account.
     * Assumes that the password for this account is available via {@link PasswordUtil#getPassword}.
     * This method does not itself count towards the rate limit.
     *
     * @param accountName the account for which to get the rate limit
     * @return see above
     */
    public static GitHubRateLimit getRateLimit(final String accountName) {
        final GitHubRESTClient restClient = new GitHubRESTClientImpl();
        final String password = getPassword(accountName);
        if (password == null) {
            throw new IllegalStateException(format("No stored password for GitHub account '%s'", accountName));
        }
        return restClient.getRateLimit(accountName, password);
    }

    private GithubTestHelper() {}
}
