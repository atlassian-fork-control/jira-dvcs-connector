package it.restart.com.atlassian.jira.plugins.dvcs.test;

import com.atlassian.jira.plugins.dvcs.pageobjects.common.BitbucketTestedProduct;
import com.atlassian.jira.plugins.dvcs.pageobjects.common.OAuth;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.AccountType;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.OAuthCredentials;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.BitbucketRemoteClient;
import com.atlassian.plugins.navlink.consumer.menu.rest.MenuNavigationLinkEntity;
import it.com.atlassian.jira.plugins.dvcs.DvcsWebDriverTestCase;
import it.com.atlassian.jira.plugins.dvcs.navlinks.NavLinksClient;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static com.atlassian.jira.plugins.dvcs.util.PasswordUtil.getPassword;
import static com.atlassian.plugin.PluginState.ENABLED;
import static it.com.atlassian.jira.plugins.PluginUtil.assertPluginState;
import static it.restart.com.atlassian.jira.plugins.dvcs.test.IntegrationTestUserDetails.ACCOUNT_NAME;
import static it.util.TestAccounts.JIRA_BB_CONNECTOR_ACCOUNT;
import static it.util.TestUtils.toOAuth;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;

/**
 * Service-level test of the {@link com.atlassian.jira.plugins.dvcs.navlinks.BitbucketCloudAccountNavLinkRepository}.
 */
public class BitbucketCloudAccountNavLinkRepositoryTest extends DvcsWebDriverTestCase {

    private static final BitbucketRemoteClient BITBUCKET_CLIENT =
            new BitbucketRemoteClient(JIRA_BB_CONNECTOR_ACCOUNT, getPassword(JIRA_BB_CONNECTOR_ACCOUNT));
    private static final BitbucketTestedProduct BITBUCKET = new BitbucketTestedProduct(JIRA.getTester());
    private static final String NAV_LINKS_PLUGIN = "com.atlassian.plugins.atlassian-nav-links-plugin";

    private static OAuthCredentials getOAuthCredentials(final OAuth oAuth) {
        return new OAuthCredentials(oAuth.key, oAuth.secret);
    }

    private NavLinksClient navLinksClient;
    private OAuth oAuth;

    @BeforeClass
    public void beforeClass() {
        assertPluginState(JIRA.backdoor(), NAV_LINKS_PLUGIN, ENABLED);
        JIRA.quickLoginAsAdmin();
        oAuth = toOAuth(BITBUCKET_CLIENT.getConsumerRemoteRestpoint().createConsumer(JIRA_BB_CONNECTOR_ACCOUNT));
        BITBUCKET.login(JIRA_BB_CONNECTOR_ACCOUNT, getPassword(JIRA_BB_CONNECTOR_ACCOUNT));
        navLinksClient = NavLinksClient.forJira(JIRA);
    }

    @BeforeMethod
    public void beforeMethod() {
        deleteAllOrganizations();
        disableOnboarding();    // to allow navigation to the dashboard
    }

    @AfterClass
    public void afterClass() {
        deleteAllOrganizations();
        BITBUCKET_CLIENT.getConsumerRemoteRestpoint().deleteConsumer(JIRA_BB_CONNECTOR_ACCOUNT, oAuth.applicationId);
        BITBUCKET.logout();
    }

    @Test
    public void connectingBitbucketAccountShouldAddNewAppSwitcherLink() {
        // Set up
        final List<MenuNavigationLinkEntity> linksBefore = navLinksClient.getAppSwitcherLinks();

        // Invoke
        addOrganization(AccountType.BITBUCKET, ACCOUNT_NAME, getOAuthCredentials(oAuth), false);

        // Check
        final List<MenuNavigationLinkEntity> linksAfter = navLinksClient.getAppSwitcherLinks();
        assertEquals(linksAfter.size(), linksBefore.size() + 1, format("Actual links: %s", linksAfter));
        linksAfter.removeAll(linksBefore);
        final MenuNavigationLinkEntity newLink = linksAfter.iterator().next();
        assertEquals(newLink.getLink(), "https://bitbucket.org/jirabitbucketconnector");
    }
}
