package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageQueueItemMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageTagMapping;
import com.atlassian.jira.plugins.dvcs.dao.impl.MessageAoDao;
import com.atlassian.jira.plugins.dvcs.dao.impl.transform.MessageEntityTransformer;
import com.atlassian.jira.plugins.dvcs.model.Message;
import com.atlassian.jira.plugins.dvcs.model.MessageId;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QMessageMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QMessageTagMapping;
import com.atlassian.jira.plugins.dvcs.service.message.MessageAddressService;
import com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessor;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessorUtils.dropTable;
import static com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessorUtils.setUpDatabaseUsingSQLFile;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MessageDaoQueryDslTest {
    protected static final String TAG_ONE = "tag1";
    protected static final String TAG_TWO = "tag2";
    private static final String AO_TABLE_NAME_PREFIX = "AO_E8B6CC";
    private static StandaloneDatabaseAccessor databaseAccessor;
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    private MessageDaoQueryDsl classUnderTest;

    private Message message;
    @Mock
    private MessageAddressService messageAddressService;
    @Mock
    private MessageAoDao messageAoDao;
    @Mock
    private Map<String, Object> messageMap;
    @Mock
    private MessageMapping messageMapping;
    @Mock
    private MessageQueueItemDaoQueryDsl messageQueueItemDaoQueryDsl;
    @Mock
    private Repository repository;

    @Before
    public void setUp() throws Exception {
        final InputStream inputStream = MessageDaoQueryDslTest.class.getResourceAsStream(
                "/com.atlassian.jira.plugins.dvcs.dao.impl.querydsl/MessageDaoQueryDslTest.sql");
        databaseAccessor = setUpDatabaseUsingSQLFile(AO_TABLE_NAME_PREFIX, inputStream,
                MessageMapping.class,
                MessageTagMapping.class,
                MessageQueueItemMapping.class);

        classUnderTest = new MessageDaoQueryDsl(
                databaseAccessor,
                messageAoDao,
                new MessageEntityTransformer(messageAddressService));


        message = new Message<>();
        message.setId(1);
        message.setAddress(new MockAddress());
        message.setPayload(MockPayload.class.getCanonicalName());
        message.setPayloadType(MockPayload.class);
        message.setPriority(0);
        message.setRetriesCount(0);
        message.setTags(new String[]{TAG_TWO});

        when(messageMapping.getID()).thenReturn(1);
        when(messageMapping.getAddress()).thenReturn("test-id");
        when(messageMapping.getPayload()).thenReturn(MockPayload.class.getCanonicalName());
        when(messageMapping.getPayloadType()).thenReturn(MockPayload.class.getCanonicalName());
        when(messageMapping.getPriority()).thenReturn(0);
        when(messageMapping.getQueuesItems()).thenReturn(new MessageQueueItemMapping[0]);
        when(messageMapping.getTags()).thenReturn(new MessageTagMapping[]{new TestTagMapping(TAG_TWO, 1)});
    }

    @After
    public void finalCleanUp() throws Exception {
        dropTable("AO_E8B6CC_MESSAGE_QUEUE_ITEM", databaseAccessor);
        dropTable("AO_E8B6CC_MESSAGE", databaseAccessor);
        dropTable("AO_E8B6CC_MESSAGE_TAG", databaseAccessor);
    }

    @Test
    public void testCreate() throws Exception {
        when(messageMapping.getID()).thenReturn(1);
        when(messageMapping.getAddress()).thenReturn(null);
        when(messageMapping.getPayload()).thenReturn(MockPayload.class.getCanonicalName());
        when(messageMapping.getPayloadType()).thenReturn(MockPayload.class.getCanonicalName());
        when(messageMapping.getPriority()).thenReturn(0);
        when(messageMapping.getQueuesItems()).thenReturn(new MessageQueueItemMapping[0]);
        when(messageMapping.getTags()).thenReturn(new MessageTagMapping[]{new TestTagMapping(TAG_TWO, 1)});
        when(messageAoDao.create(messageMap, new String[]{TAG_TWO})).thenReturn(messageMapping);

        Message result = classUnderTest.create(messageMap, new String[]{TAG_TWO});

        verify(messageAoDao).create(messageMap, new String[]{TAG_TWO});

        assertThat(result.getId(), is(1));
        assertThat(result.getPayload(), is(MockPayload.class.getCanonicalName()));
        assertThat(result.getPayloadType(), is(equalTo(MockPayload.class)));
        assertThat(result.getPriority(), is(0));
    }

    @Test
    public void testCreateTag() throws Exception {
        Message message = new Message<>();
        message.setId(1);
        message.setAddress(new MockAddress());
        message.setPayload(MockPayload.class.getCanonicalName());
        message.setPayloadType(MockPayload.class);
        message.setPriority(0);
        message.setRetriesCount(0);
        message.setTags(new String[]{TAG_TWO});


        classUnderTest.createTag(message, TAG_TWO);

        verify(messageAoDao).createTag(1, TAG_TWO);
    }

    @Test
    public void testDelete() throws Exception {
        classUnderTest.delete(message);

        long messageCountAfterDelete = databaseAccessor.runInTransaction(connection -> {
                    QMessageMapping qMessageMapping = new QMessageMapping();
                    return connection.select(qMessageMapping.ID).from(qMessageMapping).fetchCount();
                }
        );

        assertThat(messageCountAfterDelete, is(0L));

        long tagCountAfterDelete = databaseAccessor.runInTransaction(connection -> {
                    QMessageTagMapping qMessageTagMapping = new QMessageTagMapping();
                    return connection.select(qMessageTagMapping.ID).from(qMessageTagMapping).fetchCount();
                }
        );

        assertThat(tagCountAfterDelete, is(0L));
    }

    @Test
    public void testDeleteTag() throws Exception {
        classUnderTest.deleteTag(message, message.getTags()[0]);

        long messageCountAfterDelete = databaseAccessor.runInTransaction(connection -> {
                    QMessageMapping qMessageMapping = new QMessageMapping();
                    return connection.select(qMessageMapping.ID).from(qMessageMapping).fetchCount();
                }
        );

        assertThat(messageCountAfterDelete, is(1L));

        long tagCountAfterDelete = databaseAccessor.runInTransaction(connection -> {
                    QMessageTagMapping qMessageTagMapping = new QMessageTagMapping();
                    return connection.select(qMessageTagMapping.ID).from(qMessageTagMapping).fetchCount();
                }
        );

        assertThat(tagCountAfterDelete, is(0L));
    }

    @Test
    public void testGetById() throws Exception {
        when(messageAoDao.getById(1)).thenReturn(messageMapping);

        when(messageMapping.getID()).thenReturn(1);
        when(messageMapping.getAddress()).thenReturn(null);
        when(messageMapping.getPayload()).thenReturn(MockPayload.class.getCanonicalName());
        when(messageMapping.getPayloadType()).thenReturn(MockPayload.class.getCanonicalName());
        when(messageMapping.getPriority()).thenReturn(0);
        when(messageMapping.getQueuesItems()).thenReturn(new MessageQueueItemMapping[0]);
        when(messageMapping.getTags()).thenReturn(new MessageTagMapping[]{new TestTagMapping(TAG_TWO, 1)});

        Message result = classUnderTest.getById(1);

        verify(messageAoDao).getById(1);

        assertThat(result.getId(), is(1));
        assertThat(result.getPayload(), is(MockPayload.class.getCanonicalName()));
        assertThat(result.getPayloadType(), is(equalTo(MockPayload.class)));
        assertThat(result.getPriority(), is(0));
    }

    @Test
    public void testGetByTagReturnsCorrectNumber() throws Exception {
        AtomicInteger counter = new AtomicInteger(0);

        classUnderTest.getByTag(TAG_TWO, message -> counter.incrementAndGet());


        assertThat(counter.get(), is(equalTo(1)));
    }

    @Test
    public void testGetByTagCallsConsumeOntheRightThings() {
        classUnderTest.getByTag(TAG_TWO, messageId -> databaseAccessor.runInTransaction(connection ->
        {
            QMessageTagMapping qmessageTagMapping = new QMessageTagMapping();
            long messageMappingCount = connection.select(qmessageTagMapping.TAG).from(qmessageTagMapping).where(qmessageTagMapping.MESSAGE_ID.eq(messageId.getId())).fetchCount();
            assertThat(messageMappingCount, is(1l));
            return messageMappingCount;
        }));
    }

    @Test
    public void getByStateHandlesEmptyTable() {
        AtomicInteger counter = new AtomicInteger(0);
        classUnderTest.getByTag(TAG_ONE, id -> counter.incrementAndGet());

        assertThat(counter.get(), is(0));
    }

    @Test
    public void testGetMessagesForConsumingCount() throws Exception {
        classUnderTest.getMessagesForConsumingCount(TAG_TWO);
        verify(messageAoDao).getMessagesForConsumingCount(TAG_TWO);
    }

    @Test
    public void testGetTags() throws Exception {
        String[] tags = classUnderTest.getTags(new MessageId(1));

        assertThat(tags.length, is(1));
        assertThat(tags[0], is(TAG_TWO));
    }

}
