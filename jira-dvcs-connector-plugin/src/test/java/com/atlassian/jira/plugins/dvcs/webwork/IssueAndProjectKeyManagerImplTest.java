package com.atlassian.jira.plugins.dvcs.webwork;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.google.common.collect.ImmutableSet;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Set;

import static java.util.Arrays.asList;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IssueAndProjectKeyManagerImplTest {
    private static final String CURRENT_PROJECT_KEY = "project-key";
    private static final String OLD_PROJECT_KEY = "old-key";
    private static final String ORIGINAL_PROJECT_KEY = "really-old-key";
    private static final String CURRENT_ISSUE_KEY = "ISSUE-1";
    private static final String OLD_ISSUE_KEY = "OLD-2";
    private static final String ORIGINAL_ISSUE_KEY = "ANCIENT-3";
    private static final String PERMISSION_KEY = "permission";

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ProjectManager projectManager;

    @Mock
    private IssueManager issueManager;

    @Mock
    private ChangeHistoryManager changeHistoryManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @InjectMocks
    private IssueAndProjectKeyManagerImpl manager;

    @Test
    public void getAllProjectKeys_shouldReturnAllKeys_whenProjectHasHistoricKeys() {
        final Project project = createProject(1, CURRENT_PROJECT_KEY, OLD_PROJECT_KEY, ORIGINAL_PROJECT_KEY);
        final Set<String> result = manager.getAllProjectKeys(project.getKey());
        assertThat(result).containsOnly(CURRENT_PROJECT_KEY, OLD_PROJECT_KEY, ORIGINAL_PROJECT_KEY);
    }

    @Test
    public void getAllProjectKeys_shouldReturnKey_whenProjectHasNoHistoricKeys() {
        final Project project = createProject(1, CURRENT_PROJECT_KEY);
        final Set<String> result = manager.getAllProjectKeys(project.getKey());
        assertThat(result).containsOnly(CURRENT_PROJECT_KEY);
    }

    @Test
    public void getAllProjectKeys_shouldReturnEmpty_whenNonExistentProject() {
        createProject(1, CURRENT_PROJECT_KEY);
        final Set<String> result = manager.getAllProjectKeys("not-a-project");
        assertThat(result).isEmpty();
    }

    @Test
    public void getAllIssueKeys_shouldReturnAllKeys_whenIssueHasHistoricKeys() {
        final Issue issue = createIssue(100, CURRENT_ISSUE_KEY, OLD_ISSUE_KEY, ORIGINAL_ISSUE_KEY);
        final Set<String> result = manager.getAllIssueKeys(issue.getKey());
        assertThat(result).containsOnly(CURRENT_ISSUE_KEY, OLD_ISSUE_KEY, ORIGINAL_ISSUE_KEY);
    }

    @Test
    public void getAllIssueKeys_shouldReturnKey_whenIssueHasNoHistoricKeys() {
        final Issue issue = createIssue(100, CURRENT_ISSUE_KEY);
        final Set<String> result = manager.getAllIssueKeys(issue.getKey());
        assertThat(result).containsOnly(CURRENT_ISSUE_KEY);
    }

    @Test
    public void getAllIssueKeys_shouldReturnEmpty_whenNonExistentIssue() {
        createIssue(100, CURRENT_ISSUE_KEY);
        final Set<String> result = manager.getAllIssueKeys("not-the-issue");
        assertThat(result).isEmpty();
    }

    @Test
    public void hasProjectPermission_shouldReturnTrue_whenHasPermission() {
        final Project project = createProject(1, CURRENT_PROJECT_KEY);
        final ProjectPermissionKey permissionKey = new ProjectPermissionKey(PERMISSION_KEY);
        grantProjectPermission(project, permissionKey);

        assertThat(manager.hasProjectPermission(permissionKey, project)).isTrue();
    }

    @Test
    public void hasProjectPermission_shouldReturnFalse_whenDoesNotHavePermission() {
        final Project project = createProject(1, CURRENT_PROJECT_KEY);
        grantProjectPermission(project, new ProjectPermissionKey(PERMISSION_KEY));

        final ProjectPermissionKey permissionKey = new ProjectPermissionKey("no-permission");

        assertThat(manager.hasProjectPermission(permissionKey, project)).isFalse();
    }

    @Test(expected = IllegalArgumentException.class)
    public void hasProjectPermission_shouldFail_whenProjectIsNull() {
        final Project project = null;
        final ProjectPermissionKey permissionKey = new ProjectPermissionKey(PERMISSION_KEY);

        manager.hasProjectPermission(permissionKey, project);
    }

    @Test
    public void hasIssuePermission_shouldReturnTrue_whenHasPermission() {
        final Issue issue = createIssue(1, CURRENT_ISSUE_KEY);
        final ProjectPermissionKey permissionKey = new ProjectPermissionKey(PERMISSION_KEY);
        grantIssuePermission(issue, permissionKey);

        assertThat(manager.hasIssuePermission(permissionKey, issue)).isTrue();
    }

    @Test
    public void hasIssuePermission_shouldReturnFalse_whenDoesNotHavePermission() {
        final Issue issue = createIssue(1, CURRENT_ISSUE_KEY);
        grantIssuePermission(issue, new ProjectPermissionKey(PERMISSION_KEY));

        final ProjectPermissionKey permissionKey = new ProjectPermissionKey("no-permission");

        assertThat(manager.hasIssuePermission(permissionKey, issue)).isFalse();
    }

    @Test(expected = IllegalArgumentException.class)
    public void hasIssuePermission_shouldFail_whenIssueIsNull() {
        final Issue issue = null;
        final ProjectPermissionKey permissionKey = new ProjectPermissionKey(PERMISSION_KEY);

        manager.hasIssuePermission(permissionKey, issue);
    }

    private void grantIssuePermission(final Issue issue, final ProjectPermissionKey permissionKey) {
        when(permissionManager.hasPermission(eq(permissionKey), eq(issue), any())).thenReturn(true);
    }

    private void grantProjectPermission(final Project project, final ProjectPermissionKey permissionKey) {
        when(permissionManager.hasPermission(eq(permissionKey), eq(project), any())).thenReturn(true);
    }

    private Project createProject(final long id, final String key, final String... historicKeys) {
        final Project project = mock(Project.class);
        when(project.getId()).thenReturn(id);
        when(project.getKey()).thenReturn(key);

        when(projectManager.getProjectObjByKey(key)).thenReturn(project);
        when(projectManager.getAllProjectKeys(id)).thenReturn(
                ImmutableSet.<String>builder().add(key).addAll(asList(historicKeys)).build());

        return project;
    }

    private Issue createIssue(final long id, final String key, final String... historicKeys) {
        final MutableIssue issue = mock(MutableIssue.class);
        when(issue.getId()).thenReturn(id);
        when(issue.getKey()).thenReturn(key);

        when(issueManager.getIssueObject(key)).thenReturn(issue);
        when(issueManager.getAllIssueKeys(id)).thenReturn(
                ImmutableSet.<String>builder().add(key).addAll(asList(historicKeys)).build());

        return issue;
    }
}
