package com.atlassian.jira.plugins.dvcs.rest;


import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.plugins.dvcs.rest.external.v1.DevSummaryChangedEventResource;
import com.atlassian.jira.plugins.dvcs.service.admin.DevSummaryCachePrimingStatus;
import com.atlassian.jira.plugins.dvcs.service.admin.DevSummaryChangedEventService;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.jira.permission.GlobalPermissionKey.SYSTEM_ADMIN;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

public class DevSummaryChangedEventResourceTest {
    private static final int PAGE_SIZE = 100;

    @Mock
    private ApplicationUser applicationUser;

    @Mock
    private DevSummaryChangedEventService devSummaryChangedEventService;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private GlobalPermissionManager globalPermissionManager;

    @Mock
    private FeatureManager featureManager;

    private DevSummaryChangedEventResource devSummaryChangedEventResource;

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(authenticationContext.getLoggedInUser()).thenReturn(applicationUser);
        devSummaryChangedEventResource = new DevSummaryChangedEventResource(featureManager, globalPermissionManager,
                authenticationContext, devSummaryChangedEventService);
        ReflectionTestUtils.setField(devSummaryChangedEventResource, "devSummaryChangedEventService", devSummaryChangedEventService);
        setUpSystemAdmin(true);
        when(featureManager.isOnDemand()).thenReturn(true);
    }

    private void setUpSystemAdmin(final boolean isSystemAdmin) {
        when(globalPermissionManager.hasPermission(SYSTEM_ADMIN, applicationUser)).thenReturn(isSystemAdmin);
    }

    @Test
    public void testStartPrimingSuccess() {
        when(devSummaryChangedEventService.generateDevSummaryEvents(PAGE_SIZE)).thenReturn(true);
        Response response = devSummaryChangedEventResource.startGeneration(PAGE_SIZE);
        assertThat(response.getStatus(), is(Status.OK.getStatusCode()));
    }

    @Test
    public void testStartPrimingFailure() {
        when(devSummaryChangedEventService.generateDevSummaryEvents(PAGE_SIZE)).thenReturn(false);
        Response response = devSummaryChangedEventResource.startGeneration(PAGE_SIZE);
        assertThat(response.getStatus(), is(Status.CONFLICT.getStatusCode()));
    }

    @Test
    public void testStartPrimingNonAdmin() {
        setUpSystemAdmin(false);
        Response response = devSummaryChangedEventResource.startGeneration(PAGE_SIZE);
        assertThat(response.getStatus(), is(Status.UNAUTHORIZED.getStatusCode()));
    }

    @Test
    public void testStartPrimingNonOD() {
        when(featureManager.isOnDemand()).thenReturn(false);
        Response response = devSummaryChangedEventResource.startGeneration(PAGE_SIZE);
        assertThat(response.getStatus(), is(Status.FORBIDDEN.getStatusCode()));
    }

    @Test
    public void testStatus() {
        when(devSummaryChangedEventService.getEventGenerationStatus()).thenReturn(new DevSummaryCachePrimingStatus());
        Response response = devSummaryChangedEventResource.generationStatus();
        assertThat(response.getStatus(), is(Status.OK.getStatusCode()));
    }

    @Test
    public void testStatusNonAdmin() {
        setUpSystemAdmin(false);
        Response response = devSummaryChangedEventResource.generationStatus();
        assertThat(response.getStatus(), is(Status.UNAUTHORIZED.getStatusCode()));
    }

    @Test
    public void testStop() {

        Response response = devSummaryChangedEventResource.stopGeneration();
        assertThat(response.getStatus(), is(Status.OK.getStatusCode()));
    }

    @Test
    public void testStopNonAdmin() {
        setUpSystemAdmin(false);
        Response response = devSummaryChangedEventResource.stopGeneration();
        assertThat(response.getStatus(), is(Status.UNAUTHORIZED.getStatusCode()));
    }
}
