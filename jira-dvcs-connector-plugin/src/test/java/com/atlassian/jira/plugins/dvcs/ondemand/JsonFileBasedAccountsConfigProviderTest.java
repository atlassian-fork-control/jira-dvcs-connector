package com.atlassian.jira.plugins.dvcs.ondemand;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.plugins.dvcs.ondemand.AccountsConfig.BitbucketAccountInfo;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.NoSuchElementException;

import static com.atlassian.jira.config.CoreFeatures.ON_DEMAND;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class JsonFileBasedAccountsConfigProviderTest {
    @Mock
    private FeatureManager featureManager;
    @Mock
    private JiraProperties jiraProperties;

    @Nonnull
    private static BitbucketAccountInfo getFirstBitbucketAccountConfig(@Nonnull final AccountsConfig configuration)
            throws NoSuchElementException {
        return configuration
                .getSysadminApplicationLinks()
                .stream()
                .findFirst()
                .flatMap(links -> links.getBitbucket().stream().findFirst())
                .orElseThrow(() -> new NoSuchElementException("No Bitbucket account configs in " + configuration));
    }

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        enableIntegratedAccounts();
    }

    private void enableIntegratedAccounts() {
        when(featureManager.isEnabled(ON_DEMAND)).thenReturn(true);
    }

    @Test
    public void testJsonReadWithSuccess() {
        // Set up
        final AccountsConfigProvider provider = getCustomProvider("ondemand/ondemand.properties");

        // Invoke
        final AccountsConfig configuration = provider.provideConfiguration();

        // Check
        assert configuration != null;
        final BitbucketAccountInfo linkConfig = getFirstBitbucketAccountConfig(configuration);
        assertThat(linkConfig.getAccount()).isEqualTo("mybucketbit");
        assertThat(linkConfig.getKey()).isEqualTo("verysecretkey");
        assertThat(linkConfig.getSecret()).isEqualTo("verysecretsecret");
    }

    @Test
    public void testJsonReadWithSuccessMoreData() {
        // Set up
        final AccountsConfigProvider provider = getCustomProvider("ondemand/ondemand-more-data.properties");

        // Invoke
        final AccountsConfig configuration = provider.provideConfiguration();

        // Check
        assert configuration != null;
        final BitbucketAccountInfo linkConfig = getFirstBitbucketAccountConfig(configuration);
        assertThat(linkConfig.getAccount()).isEqualTo("mybucketbit");
        assertThat(linkConfig.getKey()).isEqualTo("verysecretkey");
        assertThat(linkConfig.getSecret()).isEqualTo("verysecretsecret");
    }

    @Test
    public void testJsonReadInvalidFile() {
        // Set up
        final AccountsConfigProvider provider = getCustomProvider("ondemand/ondemand-failed-content.properties");

        // Invoke
        final AccountsConfig configuration = provider.provideConfiguration();

        // Check
        assertThat(configuration).isNull();
    }

    @Test
    public void testJsonReadFileNotFound() {
        // Set up
        final AccountsConfigProvider provider = getCustomProvider("/ondemand/ondemand-NOTFOUND.properties");

        // Invoke
        final AccountsConfig configuration = provider.provideConfiguration();

        // Check
        assertThat(configuration).isNull();
    }

    private AccountsConfigProvider getCustomProvider(final String pathWithinTestResources) {
        return getCustomProviderAbsolutePath(getFileURL(pathWithinTestResources));
    }

    private AccountsConfigProvider getCustomProviderAbsolutePath(final URL configFileUrl) {
        return new JsonFileBasedAccountsConfigProvider(featureManager, jiraProperties) {
            @Override
            File getConfigFile() {
                try {
                    return new File(configFileUrl.toURI());
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }

    private URL getFileURL(final String pathWithinTestResources) {
        return getClass().getClassLoader().getResource(pathWithinTestResources);
    }
}

