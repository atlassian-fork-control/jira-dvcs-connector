package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageQueueItemMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageTagMapping;
import com.atlassian.jira.plugins.dvcs.event.SyncEventMapping;
import net.java.ao.EntityManager;

import java.sql.SQLException;

/**
 * Utility class that ensure that all tables required for the static table helper are initializes
 */
public class InitTables {

    public static void create(EntityManager entityManager) throws SQLException {
        entityManager.migrate(SyncEventMapping.class);
        entityManager.migrate(MessageMapping.class);
        entityManager.migrate(MessageTagMapping.class);
        entityManager.migrate(MessageQueueItemMapping.class);
    }
}
