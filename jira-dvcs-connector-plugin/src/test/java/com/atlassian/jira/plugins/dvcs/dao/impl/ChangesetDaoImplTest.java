package com.atlassian.jira.plugins.dvcs.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.jira.plugins.dvcs.activeobjects.DvcsConnectorTableNameConverter;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.ChangesetMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.IssueToChangesetMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.OrganizationMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryToChangesetMapping;
import com.atlassian.jira.plugins.dvcs.ao.QueryHelper;
import com.atlassian.jira.plugins.dvcs.dao.IssueToMappingFunction;
import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.jira.plugins.dvcs.model.GlobalFilter;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import net.java.ao.EntityManager;
import net.java.ao.test.converters.NameConverters;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.DatabaseUpdater;
import net.java.ao.test.jdbc.DynamicJdbcConfiguration;
import net.java.ao.test.jdbc.Jdbc;
import net.java.ao.test.jdbc.NonTransactional;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;
import org.apache.commons.io.LineIterator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.collect.ImmutableList.of;
import static java.util.Arrays.asList;
import static org.apache.commons.io.IOUtils.lineIterator;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;

@RunWith(ActiveObjectsJUnitRunner.class)
@Jdbc(DynamicJdbcConfiguration.class)
@Data(ChangesetDaoImplTest.DatabaseSeeder.class)
@NameConverters(table = DvcsConnectorTableNameConverter.class)
public class ChangesetDaoImplTest {
    public static final String ISSUE_WITH_NO_CHANGESETS = "TEST-123";
    private static final int LARGE_PAGE_SIZE = 100;
    private static final int SMALL_PAGE_SIZE = 1;
    // --------
    // Reflection of the test data defined in ChangesetDaoImplTest.sql
    // --------
    private static final Organization ORGANIZATION = createOrganization(1, "bitbucket");
    private static final Repository REPOSITORY = createRepository(10);
    private static final String EXISTING_NODE = "11111111111111111111111111111111";
    private static final String NEW_NODE = "new-node-111111111111111";
    private static final String[] PROJECT_KEYS = {"TEST", "FOO"};
    private static final String[] ISSUE_KEYS = {"TEST-3", "TEST-10", "TEST-15"};
    private static final String[] AUTHORS = {"testauthor1", "testauthor2", "testauthor3", "testauthor4", "testauthor5"};
    private static final Map<String, String[]> NODES_BY_ISSUE;
    private static final Map<String, String[]> NODES_BY_PROJECT;
    private static final Map<String, String[]> NODES_BY_AUTHOR;
    private static final String[] ALL_NODES = {
            "11111111111111111111111111111111",
            "22222222222222222222222222222222",
            "33333333333333333333333333333333",
            "44444444444444444444444444444444",
            "55555555555555555555555555555555",
            "66666666666666666666666666666666",
            "77777777777777777777777777777777",
            "88888888888888888888888888888888",
            "99999999999999999999999999999999"
    };

    // --------

    private static final Set<String> NEW_ISSUE_KEYS = ImmutableSet.of("ISSUE-1");

    static {
        NODES_BY_ISSUE = ImmutableMap.of(
                "TEST-3", new String[]{
                        "11111111111111111111111111111111",
                        "22222222222222222222222222222222",
                        "33333333333333333333333333333333",
                        "44444444444444444444444444444444",
                        "55555555555555555555555555555555",
                        "66666666666666666666666666666666",
                        "77777777777777777777777777777777"
                },
                "TEST-10", new String[]{
                        "88888888888888888888888888888888"
                },
                "TEST-15", new String[]{
                        "99999999999999999999999999999999"
                }
        );
        NODES_BY_PROJECT = ImmutableMap.of(
                "TEST", new String[]{
                        "11111111111111111111111111111111",
                        "22222222222222222222222222222222",
                        "33333333333333333333333333333333",
                        "55555555555555555555555555555555",
                        "66666666666666666666666666666666",
                        "88888888888888888888888888888888"
                },
                "FOO", new String[]{
                        "44444444444444444444444444444444",
                        "77777777777777777777777777777777",
                        "99999999999999999999999999999999"
                }
        );
        NODES_BY_AUTHOR = ImmutableMap.of(
                "testauthor1", new String[]{
                        "11111111111111111111111111111111",
                },
                "testauthor2", new String[]{
                        "22222222222222222222222222222222",
                        "33333333333333333333333333333333"
                },
                "testauthor3", new String[]{
                        "44444444444444444444444444444444",
                        "55555555555555555555555555555555",
                },
                "testauthor4", new String[]{
                        "66666666666666666666666666666666",
                        "77777777777777777777777777777777"
                },
                "testauthor5", new String[]{
                        "88888888888888888888888888888888",
                        "99999999999999999999999999999999"
                }
        );
    }

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();
    private EntityManager entityManager;
    private ActiveObjects activeObjects;
    @Mock
    private QueryHelper queryHelper;

    private ChangesetDaoImpl changesetDao;

    private static Changeset createChangeset(final String node) {
        final Changeset changeset = new Changeset(REPOSITORY.getId(), node, "A message", new Date());
        changeset.setRawNode(node);
        return changeset;
    }

    private static Repository createRepository(final int id) {
        final Repository result = new Repository();
        result.setId(id);
        return result;
    }

    private static Organization createOrganization(final int id, final String dvcsType) {
        final Organization result = new Organization();
        result.setId(id);
        result.setDvcsType(dvcsType);
        return result;
    }

    private static void assertContainsExpectedNodes(final List<Changeset> changesets, final String[]... nodeCollections) {
        final List<String> changesetNodes = changesets.stream().map(Changeset::getNode).collect(Collectors.toList());
        List<String> nodes = new ArrayList<>();
        for (String[] collection : nodeCollections) {
            nodes.addAll(asList(collection));
        }
        assertThat(changesetNodes, containsInAnyOrder(nodes.toArray()));
    }

    @Before
    public void setUp() throws Exception {
        assertNotNull(entityManager);
        activeObjects = new TestActiveObjects(entityManager);

        changesetDao = new ChangesetDaoImpl(activeObjects, queryHelper);
    }

    @Test
    @NonTransactional
    public void createOrAssociate_shouldReturnTrue_forNewChangeset() throws Exception {
        final Changeset changeset = createChangeset(NEW_NODE);

        boolean isNew = changesetDao.createOrAssociate(changeset, NEW_ISSUE_KEYS);
        assertThat(isNew, equalTo(true));
    }

    @Test
    @NonTransactional
    public void createOrAssociate_shouldReturnFalse_forExistingChangeset() throws Exception {
        final Changeset changeset = createChangeset(EXISTING_NODE);

        boolean isNew = changesetDao.createOrAssociate(changeset, NEW_ISSUE_KEYS);
        assertThat(isNew, equalTo(false));
    }

    @Test
    public void forEachIssueKey_processesAllKeys_withLargePageSize() throws Exception {
        final TestIssueKeyFunction testFunction = new TestIssueKeyFunction();

        boolean result = changesetDao.forEachIssueKeyMapping(ORGANIZATION, REPOSITORY, LARGE_PAGE_SIZE, testFunction);

        assertThat(result, is(true));
        assertThat(testFunction.pageCount, equalTo(2)); // We always grab an empty page at the end to terminate the iteration
        assertThat(testFunction.processedIssueKeys, contains(ISSUE_KEYS));
    }

    @Test
    public void forEachIssueKey_processesAllKeys_withSmallPageSize() throws Exception {
        final TestIssueKeyFunction testFunction = new TestIssueKeyFunction();

        boolean result = changesetDao.forEachIssueKeyMapping(ORGANIZATION, REPOSITORY, SMALL_PAGE_SIZE, testFunction);

        assertThat(result, is(true));
        assertThat(testFunction.pageCount, equalTo(10)); // We always grab an empty page at the end to terminate the iteration
        assertThat(testFunction.processedIssueKeys, contains(ISSUE_KEYS));
    }

    @Test
    public void forEachIssueKey_stopsProcessing_whenFunctionReturnsFalse() throws Exception {
        final TestIssueKeyFunction testFunction = new TestIssueKeyFunction(2);

        boolean result = changesetDao.forEachIssueKeyMapping(ORGANIZATION, REPOSITORY, SMALL_PAGE_SIZE, testFunction);

        assertThat(result, is(false));
        assertThat(testFunction.pageCount, equalTo(2));
        assertThat(testFunction.processedIssueKeys, contains(ISSUE_KEYS[0]));
    }

    @Test
    @NonTransactional
    public void getByIssueKey_returnsEmptyList_whenNoChangesetsExistForIssueKey() {
        final List<Changeset> changesets = changesetDao.getByIssueKey(of(ISSUE_WITH_NO_CHANGESETS), false);

        assertThat(changesets.isEmpty(), is(true));
    }

    @Test
    @NonTransactional
    public void getByIssueKey_returnsChangesets_whenChangesetsExistForIssueKey() {
        final List<Changeset> changesets = changesetDao.getByIssueKey(of(ISSUE_KEYS[0]), false);

        assertContainsExpectedNodes(changesets, NODES_BY_ISSUE.get(ISSUE_KEYS[0]));
    }

    @Test
    @NonTransactional
    public void getByIssueKey_returnsCombinedChangesets_whenMultipleIssueKeys() {
        final List<Changeset> changesets = changesetDao.getByIssueKey(of(ISSUE_KEYS[0], ISSUE_KEYS[1]), false);

        assertContainsExpectedNodes(changesets, NODES_BY_ISSUE.get(ISSUE_KEYS[0]), NODES_BY_ISSUE.get(ISSUE_KEYS[1]));
    }

    @Test
    @NonTransactional
    public void getByIssueKey_returnsAllChangesets_whenNoIssueKeysProvided() {
        final List<Changeset> changesets = changesetDao.getByIssueKey(of(), false);

        assertContainsExpectedNodes(changesets, ALL_NODES);
    }

    @Test
    @NonTransactional
    public void getLatestChangesets_returnsEmptyList_whenMaxResultsIsZero() {
        final GlobalFilter globalFilter = new GlobalFilter();

        final List<Changeset> changesets = changesetDao.getLatestChangesets(0, globalFilter);

        assertThat(changesets.isEmpty(), is(true));
    }

    @Test
    @NonTransactional
    public void getLatestChangesets_returnsAllChangesets_whenFilterIsEmpty() {
        final GlobalFilter globalFilter = new GlobalFilter();

        final List<Changeset> changesets = changesetDao.getLatestChangesets(100, globalFilter);

        assertContainsExpectedNodes(changesets, ALL_NODES);
    }

    @Test
    @NonTransactional
    public void getLatestChangesets_returnsLimitedChangesets_whenMaxResultsProvided() {
        final GlobalFilter globalFilter = new GlobalFilter();

        final List<Changeset> changesets = changesetDao.getLatestChangesets(3, globalFilter);

        assertContainsExpectedNodes(changesets,
                new String[]{ALL_NODES[0], ALL_NODES[1], ALL_NODES[2]});
    }

    @Test
    @NonTransactional
    public void getLatestChangesets_returnsCorrectChangesets_whenIssueInFilterSpecified() {
        final GlobalFilter globalFilter = new GlobalFilter();
        globalFilter.setInIssues(asList(ISSUE_KEYS[1], ISSUE_KEYS[2]));

        final List<Changeset> changesets = changesetDao.getLatestChangesets(100, globalFilter);

        assertContainsExpectedNodes(changesets, NODES_BY_ISSUE.get(ISSUE_KEYS[1]), NODES_BY_ISSUE.get(ISSUE_KEYS[2]));
    }

    @Test
    @NonTransactional
    public void getLatestChangesets_returnsCorrectChangesets_whenIssueNotInFilterSpecified() {
        final GlobalFilter globalFilter = new GlobalFilter();
        globalFilter.setNotInIssues(asList(ISSUE_KEYS[1], ISSUE_KEYS[2]));

        final List<Changeset> changesets = changesetDao.getLatestChangesets(100, globalFilter);

        assertContainsExpectedNodes(changesets, NODES_BY_ISSUE.get(ISSUE_KEYS[0]));
    }

    @Test
    @NonTransactional
    public void getLatestChangesets_returnsCorrectChangesets_whenProjectInFilterSpecified() {
        final GlobalFilter globalFilter = new GlobalFilter();
        globalFilter.setInProjects(asList(PROJECT_KEYS[0]));

        final List<Changeset> changesets = changesetDao.getLatestChangesets(100, globalFilter);

        assertContainsExpectedNodes(changesets, NODES_BY_PROJECT.get(PROJECT_KEYS[0]));
    }

    @Test
    @NonTransactional
    public void getLatestChangesets_returnsCorrectChangesets_whenProjectNotInFilterSpecified() {
        final GlobalFilter globalFilter = new GlobalFilter();
        globalFilter.setNotInProjects(asList(PROJECT_KEYS[0]));

        final List<Changeset> changesets = changesetDao.getLatestChangesets(100, globalFilter);

        assertContainsExpectedNodes(changesets, NODES_BY_PROJECT.get(PROJECT_KEYS[1]));
    }

    @Test
    @NonTransactional
    public void getLatestChangesets_returnsCorrectChangesets_whenUserInFilterSpecified() {
        final GlobalFilter globalFilter = new GlobalFilter();
        globalFilter.setInUsers(asList(AUTHORS[0], AUTHORS[3]));

        final List<Changeset> changesets = changesetDao.getLatestChangesets(100, globalFilter);

        assertContainsExpectedNodes(changesets, NODES_BY_AUTHOR.get(AUTHORS[0]), NODES_BY_AUTHOR.get(AUTHORS[3]));
    }

    @Test
    @NonTransactional
    public void getLatestChangesets_returnsCorrectChangesets_whenUserNotInFilterSpecified() {
        final GlobalFilter globalFilter = new GlobalFilter();
        globalFilter.setNotInUsers(asList(AUTHORS[0], AUTHORS[3]));

        final List<Changeset> changesets = changesetDao.getLatestChangesets(100, globalFilter);

        assertContainsExpectedNodes(changesets,
                NODES_BY_AUTHOR.get(AUTHORS[1]), NODES_BY_AUTHOR.get(AUTHORS[2]), NODES_BY_AUTHOR.get(AUTHORS[4]));
    }

    /**
     * A simple {@link IssueToMappingFunction} that captures information about what
     * was passed to it during test execution
     */
    private static class TestIssueKeyFunction implements IssueToMappingFunction {
        final Set<String> processedIssueKeys = new HashSet<>();
        Integer stopAfterPage = null;
        int pageCount = 0;

        public TestIssueKeyFunction(final int stopAfterPage) {
            this.stopAfterPage = stopAfterPage;
        }

        public TestIssueKeyFunction() {
        }

        @Override
        public boolean execute(final String dvcsType, final int repositoryId, final Set<String> issueKeys) {
            pageCount++;
            processedIssueKeys.addAll(issueKeys);

            return stopAfterPage == null || stopAfterPage > pageCount;
        }
    }

    public static class DatabaseSeeder implements DatabaseUpdater {
        @Override
        public void update(final EntityManager em) throws Exception {
            em.migrate(ChangesetMapping.class,
                    OrganizationMapping.class,
                    RepositoryMapping.class,
                    IssueToChangesetMapping.class,
                    RepositoryToChangesetMapping.class);

            final JdbcOperations jdbcTemplate = new JdbcTemplate(new SingleConnectionDataSource(em.getProvider().getConnection(), true));

            final LineIterator lineIterator = lineIterator(getClass().getResourceAsStream("ChangesetDaoImplTest.sql"), "UTF-8");
            try {
                while (lineIterator.hasNext()) {
                    final String statement = lineIterator.nextLine().trim();
                    if (!isBlank(statement)) {
                        jdbcTemplate.execute(statement);
                    }
                }
            } finally {
                lineIterator.close();
            }
        }
    }
}
