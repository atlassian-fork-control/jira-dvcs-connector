/**
 * Supported DVCS types.
 *
 * @readonly
 * @enum {string}
 */
var DvcsType = {
    BITBUCKET: 'bitbucket',
    GITHUB: 'github',
    GITHUB_ENTERPRISE: 'githube'
};

/**
 * Supported DVCS providers, keyed by DvcsType
 * @readonly
 */
var DvcsProviders = {
    bitbucket: {
        actionPath: '/secure/admin/AddBitbucketOrganization.jspa',
        hostUrl: function () {
            if (dvcs.connector.plugin.bitbucketOverrideUrl) {
                return dvcs.connector.plugin.bitbucketOverrideUrl;
            }
            return 'https://bitbucket.org';
        }
    },
    github: {
        actionPath: '/secure/admin/AddGithubOrganization.jspa',
        hostUrl: function () {
            return 'https://github.com';
        }
    },
    githube: {
        actionPath: '/secure/admin/AddGithubEnterpriseOrganization.jspa',
        hostUrl: function () {
            return 'https://github.com';
        }
    }
};

var DvcsConnectorRestClient = require('jira-dvcs-connector/rest/dvcs-connector-rest-client');
var BitbucketClient = require('jira-dvcs-connector/bitbucket/rest/bitbucket-client');
var Navigate = require('jira-dvcs-connector/util/navigate');
var ConfigureOrganization = require('jira-dvcs-connector/admin/configure-organization');
var updateSyncStatus = require('jira-dvcs-connector/ui/update-sync-status');

/**
 * Switch the Add Organization dialog to the selected DVCS type mode.
 *
 * @param {DvcsType} dvcsType The dvcs type selected
 */
function switchDvcsDetails(dvcsType) {
    // clear all form errors
    DvcsValidator.clearAllErrors();

    AJS.$("#url").val(DvcsProviders[dvcsType].hostUrl());
    AJS.$("#organization").focus().select();
    AJS.$("#repoEntry").attr("action", BASE_URL + DvcsProviders[dvcsType].actionPath);

    if (dvcsType == DvcsType.BITBUCKET) {

        AJS.$('#github-form-section').hide();
        AJS.$('#githube-form-section').hide();
        AJS.$("#bitbucket-form-section").fadeIn();

    } else if (dvcsType == DvcsType.GITHUB) {

        AJS.$('#bitbucket-form-section').hide();
        AJS.$('#githube-form-section').hide();
        AJS.$("#github-form-section").fadeIn();

    } else if (dvcsType == DvcsType.GITHUB_ENTERPRISE) {

        AJS.$('#bitbucket-form-section').hide();
        AJS.$('#github-form-section').hide();
        AJS.$("#githube-form-section").fadeIn();
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

function forceSync(event, repositoryId) {
    if (event.shiftKey) {
        var dialogTrigger = AJS.$("#jira-dvcs-connector-forceSyncDialog-" + repositoryId);
        var dialog = dialogTrigger.data('jira-dvcs-connector-forceSyncDialog');

        if (!dialog) {
            dialog = AJS.InlineDialog(AJS.$("#jira-dvcs-connector-forceSyncDialog-" + repositoryId), "jira-dvcs-connector-forceSyncDialog" + repositoryId, function (content, trigger, showPopup) {
                content.html(dvcs.connector.plugin.soy.forceSyncDialog({
                    'repositoryId': repositoryId
                }));
                showPopup();
                return false;
            }, {width: 500, hideDelay: null, noBind: true});
            dialogTrigger.data('jira-dvcs-connector-forceSyncDialog', dialog);
        }
        dialog.show();

    } else {
        softSync(repositoryId);
    }
}

function fullSync(repositoryId) {
    AJS.$.post(BASE_URL + "/rest/bitbucket/1.0/repository/" + repositoryId + "/fullsync", function (data) {
        updateSyncStatus(data);
    });
}

function softSync(repositoryId) {
    AJS.$.post(BASE_URL + "/rest/bitbucket/1.0/repository/" + repositoryId + "/softsync", function (data) {
        updateSyncStatus(data);
    });
}

function retrieveSyncStatus() {
    AJS.$.getJSON(BASE_URL + "/rest/bitbucket/1.0/repositories", function (data) {
        AJS.$.each(data.repositories, function (a, repo) {
            updateSyncStatus(repo);
        });
        window.setTimeout(retrieveSyncStatus, 4000);
    }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        dvcsLogConsole("Request Failed: " + err);
        window.setTimeout(retrieveSyncStatus, 40000);
    });
}

function getLastCommitRelativeDateHtml(daysAgo) {
    var html = "";
    if (daysAgo) {
        html = new Date(daysAgo).toDateString();
    }
    return html;
}

/**
 * Redirect the user's browser to the Bitbucket authorize addon screen to initiate the
 * addon installation flow.
 *
 * @param {String} [bitbucketBaseUrl] The URL to use to override the location of bitbucket for testing etc. If not
 * provided, will use dvcs.connector.plugin.bitbucketOverrideUrl or the default URL found in DvcsProviders.
 */
function redirectToBitbucketAuthorize(bitbucketBaseUrl) {
    if (!bitbucketBaseUrl) {
        bitbucketBaseUrl = DvcsProviders[DvcsType.BITBUCKET].hostUrl();
    }
    var jiraBaseUrl = Navigate.getOrigin() + BASE_URL;
    var descriptorUri = jiraBaseUrl + '/rest/aci/1.0/installation/jira-bitbucket-connector-plugin/descriptor';
    var redirectUri = jiraBaseUrl + '/secure/BitbucketPostInstallApprovalAction.jspa?jiraInitiated=true&atl_token='
        + dvcs.connector.plugin.atlToken;

    triggerAnalyticsEvent("add.bitbucket.flow.started");

    var client = new BitbucketClient(bitbucketBaseUrl);

    Navigate.navigate(client.addons.generateAddonAuthorizeRedirect(descriptorUri, redirectUri));
}

function showAddRepoDetails(show, hostToSelect) {
    if (!dvcs.connector.plugin.addOrganizationDialog) {
        createAddOrganizationDialog();
    }
    var dialog = dvcs.connector.plugin.addOrganizationDialog;
    // Reset to default view:
    AJS.$('#repoEntry').attr("action", "");
    // - hide username/password
    AJS.$("#github-form-section").hide();

    // - show url, organization field
    var urlSelect = AJS.$('#urlSelect');
    urlSelect.show();
    /**
     * Building an internal map of all of the available hosts to avoid the use of AJS.$ or .find
     * in the case of potential XSS hole when mixing input from url with query string
     */
    var availableHosts = {};
    var defaultHost;
    urlSelect.find("option").each(function (index, option) {
        var $option = AJS.$(option);
        $option.data("index", index);
        if (!dvcs.connector.plugin.disabledHosts[$option.attr("value")]) {
            var host = $option.attr("value");
            availableHosts[host] = $option;
            if (!defaultHost || host == "bitbucket") {
                defaultHost = host;
            }
        }
    });

    var selectedHost;
    if (hostToSelect && availableHosts[hostToSelect]) {
        selectedHost = AJS.$(availableHosts[hostToSelect]);
    } else {
        //Defaults to bitbucket
        selectedHost = AJS.$(availableHosts[defaultHost]);
    }

    urlSelect.val(selectedHost.attr("value"));
    AJS.$('#urlReadOnly').hide();

    AJS.$('#organization').show();
    AJS.$('#organizationReadOnly').hide();

    dialog.enabled(true);

    // clear all form errors
    DvcsValidator.clearAllErrors();

    // Enable form for the selected host
    switchDvcsDetails(selectedHost.attr("value"));

    AJS.$("#organization").focus().select();
    dialog.gotoPage(0);
    dialog.gotoPanel(0);
    dialog.show();
    dialog.updateHeight();

    triggerAnalyticsEvent("add.started");
}

function createAddOrganizationDialog(action) {
    var dialog = new AJS.Dialog({
        width: 800,
        height: 400,
        id: "add-organization-dialog",
        closeOnOutsideClick: false
    });

    // First page
    dialog.addHeader("Add New Account");

    dialog.addPanel("", dvcs.connector.plugin.soy.addOrganizationDialog({
        isOnDemandLicense: dvcs.connector.plugin.onDemandLicense,
        atlToken: dvcs.connector.plugin.atlToken,
        source: getSourceDiv().data("source"),
        disabledHosts: dvcs.connector.plugin.disabledHosts,
        isAciEnabled: dvcs.connector.plugin.aciEnabled
    }), "panel-body");

    dialog.addButtonPanel();
    dialog.page[0].buttonpanel.append("<span id='add-organization-wait' class='aui-icon' style='padding-right:10px'>&nbsp;</span>");
    dialog.addSubmit("Add", function (dialog, event) {
        if (dvcsSubmitFormHandler(event, false)) {
            AJS.$("#repoEntry").submit();
        }
    });

    dialog.addCancel("Cancel", function (dialog) {
        AJS.$("#repoEntry").trigger('reset');
        AJS.$("#aui-message-bar").empty();
        dialog.hide();
        triggerAnalyticsEvent("add.cancelled");
    }, "#");

    AJS.$('#urlSelect').change(function (event) {
        var dvcsSelect = event.target;
        var dvcsType = dvcsSelect.options[dvcsSelect.selectedIndex].value;
        switchDvcsDetails(dvcsType);
        dialog.updateHeight();
    });

    // Second page, GitHub Enterprise confirmation page
    dialog.addPage();
    dialog.addHeader("Add New Account");
    dialog.addPanel("Confirmation", "<div id='githubeConfirmation'>Test</div>", "panel-body");
    dialog.addSubmit("Continue", function (dialog, event) {
        dialog.gotoPage(0);
        dialog.updateHeight();
        if (dvcsSubmitFormHandler(event, true)) {
            AJS.$("#repoEntry").submit();
        }
    });

    dialog.addButton("Previous", function (dialog) {
        dialog.prevPage();
        dialog.updateHeight();
    });

    dialog.addCancel("Cancel", function (dialog) {
        AJS.$("#repoEntry").trigger('reset');
        AJS.$("#aui-message-bar").empty();
        dialog.hide();
        triggerAnalyticsEvent("add.cancelled");
    }, "#");

    dialog.enabled = function (enabled) {
        if (enabled) {
            AJS.$("#add-organization-wait").removeClass("aui-icon-wait");
            AJS.$('#add-organization-dialog .button-panel-submit-button').removeAttr("disabled");
            AJS.$('#add-organization-dialog .button-panel-submit-button').remove("aria-disabled");
        } else {
            AJS.$("#add-organization-wait").addClass("aui-icon-wait");
            AJS.$('#add-organization-dialog .button-panel-submit-button').attr("disabled", "disabled");
            AJS.$('#add-organization-dialog .button-panel-submit-button').attr("aria-disabled", "true");
        }
    };
    dvcs.connector.plugin.addOrganizationDialog = dialog;
}

function dvcsSubmitFormHandler(event, skipLoggingAlert) {
    var dialog = dvcs.connector.plugin.addOrganizationDialog;
    // submit form
    var organizationElement = AJS.$("#organization");
    // if not custom URL
    if (!parseAccountUrl(organizationElement.val())) {
        // some really simple validation
        if (!validateAddOrganizationForm()) {
            dialog.enabled(true);
            dialog.updateHeight();
            return false;
        }
        var selectedDvcs = AJS.$("#urlSelect option:selected");
        var dvcsHost = selectedDvcs.text();

        if (selectedDvcs.val() == DvcsType.GITHUB_ENTERPRISE) {
            // impose real URL to hidden input
            AJS.$("#url").val(AJS.$("#urlGhe").val());

            if (!skipLoggingAlert) {
                AJS.$("#githubeConfirmation").html(dvcs.connector.plugin.soy.confirmLoggedIn({
                    dvcsHost: dvcsHost
                }));
                dialog.nextPage();
                dialog.updateHeight();
                return false;
            }
        }

        // disable add form
        dialog.enabled(false);

        //
        AJS.messages.info("#aui-message-bar", {
            title: "Connecting to " + dvcsHost + " to configure your account...",
            closeable: false
        });
        dialog.updateHeight();
        // set url by selected type
        return true; // submit form
    }

    // else - lets try to identify account
    // account info
    if (!validateAccountInfoForm()) {
        dialog.enabled(true);
        return false;
    }

    var account = parseAccountUrl(AJS.$("#organization").val());
    AJS.$("#url").val(account.hostUrl);
    AJS.$("#organization").val(account.name);

    AJS.$("#aui-message-bar").empty();

    AJS.messages.info("#aui-message-bar", {title: "Trying to identify repository type...", closeable: false});
    dialog.updateHeight();

    var repositoryUrl = AJS.$("#url").val().trim();
    var organizationName = AJS.$("#organization").val().trim();

    var requestUrl = BASE_URL + "/rest/bitbucket/1.0/accountInfo?server=" + encodeURIComponent(repositoryUrl) + "&account=" + encodeURIComponent(organizationName);

    AJS.$.getJSON(requestUrl,
        function (data) {

            AJS.$("#aui-message-bar").empty();
            dialog.enabled(true);

            if (data.validationErrors && data.validationErrors.length > 0) {
                AJS.$.each(data.validationErrors, function (i, msg) {
                    AJS.messages.error("#aui-message-bar", {title: "Error!", body: msg});
                    dialog.updateHeight();
                })
            } else {
                dvcsSubmitFormAjaxHandler[data.dvcsType].apply(this, arguments);
            }
        }).error(function (a) {
        AJS.$("#aui-message-bar").empty();
        AJS.messages.error("#aui-message-bar", {
            title: "Error!",
            body: "The url [<b>" + AJS.escapeHtml(AJS.$("#url").val()) + "</b>] is incorrect or the server is not responding."
        });
        dialog.enabled(true);
        dialog.updateHeight();
    });
    return false;
}

function validateAccountInfoForm() {
    var validator = new DvcsValidator();
    validator.addItem("organization", "org-error", "required");
    return validator.runValidation();
}

function validateAddOrganizationForm() {
    var validator = new DvcsValidator();
    validator.addItem("organization", "org-error", "required");

    if (AJS.$("#oauthClientIdGhe").is(":visible")) {
        validator.addItem("urlGhe", "ghe-url-error", "required");
        validator.addItem("urlGhe", "ghe-invalid-url-error", "url");
        validator.addItem("oauthClientIdGhe", "oauth-ghe-client-error", "required");
        validator.addItem("oauthSecretGhe", "oauth-ghe-secret-error", "required");
    } else if (AJS.$("#oauthBbClientId").is(":visible")) {
        validator.addItem("oauthBbClientId", "oauth-bb-client-error", "required");
        validator.addItem("oauthBbSecret", "oauth-bb-secret-error", "required");
    } else if (AJS.$("#oauthClientId").is(":visible")) {
        validator.addItem("oauthClientId", "oauth-gh-client-error", "required");
        validator.addItem("oauthSecret", "oauth-gh-secret-error", "required");
    } else if (AJS.$("#adminUsername").is(":visible")) {
        // validator.addItem("adminUsername", "admin-username-error", "required");
        // validator.addItem("adminPassword", "admin-password-error", "required");
    }
    return validator.runValidation();
}

var dvcsSubmitFormAjaxHandler = {
    "bitbucket": function (data) {
        AJS.$("#repoEntry").attr("action", BASE_URL + "/secure/admin/AddBitbucketOrganization.jspa");
        AJS.$('#repoEntry').submit();
    },

    "github": function (data) {
        AJS.$("#repoEntry").attr("action", BASE_URL + "/secure/admin/AddGithubOrganization.jspa");
        AJS.$('#repoEntry').submit();
    }
};

function configureDefaultGroups(orgName, id) {

    var dialog = confirmationDialog({
        header: "Configure automatic access",
        body: dvcs.connector.plugin.soy.defaultGroupsForm({
            'baseUrl': BASE_URL,
            'atlToken': dvcs.connector.plugin.atlToken,
            'organizationIdDefaultGroups': id
        }),
        submitButtonLabel: "Save",
        okAction: function (dialog) {
            AJS.$("#configureDefaultGroupsForm").submit();
        }
    });

    dialog.page[0].buttonpanel.append(dvcs.connector.plugin.soy.defaultGroupsHelpUrl());
    dialog.disableActions();

    // load web fragment
    AJS.$.ajax({
            type: 'GET',
            url: BASE_URL + "/rest/bitbucket/1.0/organization/" + id + "/defaultgroups",
            success: function (data) {
                if (dialog.isAttached()) {
                    AJS.$(".dialog-panel-body #configureDefaultGroupsContent").html(dvcs.connector.plugin.soy.defaultGroups({
                        organization: data.organization,
                        groups: data.groups,
                        isBitbucketRebrandEnabled: getSourceDiv().data("bbrebrand")
                    }));
                    dialog.updateHeight();
                    dialog.enableActions();
                }
            }
        }
    ).error(function (err) {
        dialog.showError(AJS.$(err.responseXML).find('status').find('message').text());
        dialog.updateHeight();
    });
}

/**
 * Show the OAuth settings dialog for the provided org
 * <p>
 * Collects and validates oauth credentials from the user and POSTS them to the appropriate REST endpoint before
 * executing the provided success callback.
 *
 * @param org The org being configured
 * @param popupId The element ID to use for the popup dialog
 * @param header The header text to show in the dialog
 * @param submitButtonText The text to use on the dialog submit button
 * @param success A function to execute on successful submit
 */
function showOAuthDialog(org, popupId, header, submitButtonText, success) {
    /**
     * Validate that the provided required field contains a value
     *
     * @param $field The field to validate
     * @param errorMsg The message to show in case of a validation error
     * @returns {boolean} true if validation was successful; false otherwise.
     * @private
     */
    function _validateField($field, errorMsg) {
        if (!AJS.$.trim($field.val())) {
            _showFieldError($field, errorMsg);
            return false;
        }
        _clearFieldError($field);
        return true;
    }

    /**
     * Show an error against the provided dialog field
     * @param $field The field to show an error against
     * @param errorMsg The message to show
     * @private
     */
    function _showFieldError($field, errorMsg) {
        $field.next().html(errorMsg);
        $field.next().show();
    }

    /**
     * Clear the error message associated with the provided dialog field
     * @param $field The field to clear the error against
     * @private
     */
    function _clearFieldError($field) {
        $field.next().html("&nbsp;");
        $field.next().hide();
    }

    /**
     * Show an error message in the dialog
     *
     * @param {jqXHR} err The error response received from the server
     * @private
     */
    function _showOAuthErrorMsg(err) {
        var msg;
        if (err.status == 409) {
            msg = 'The credentials for this organisation are out of date and cannot be updated. ' +
                'Please <a href="javascript:location.reload()">refresh the page</a>.';
        }
        else {
            msg = "Could not configure OAuth";
        }
        AJS.$("#aui-message-bar-oauth-dialog").empty();
        AJS.messages.error("#aui-message-bar-oauth-dialog", {
            title: "Error!",
            body: msg,
            closeable: false
        });
        var $buttonPanel = AJS.$("#repositoryOAuthDialog .dialog-button-panel");
        $buttonPanel.find('button').removeAttr("disabled");
        $buttonPanel.find('button').removeAttr("aria-disabled");
        $buttonPanel.find('.aui-icon-wait').remove();
    }

    // dialog with 2 pages
    var popup = new AJS.Dialog({
        width: 600,
        height: 350,
        id: popupId
    });

    function addCancelButtonToPopup() {
        popup.addCancel(AJS.I18n.getText("common.forms.cancel"), function (dialog) {
            dialog.remove();
        });
    }

    popup.addHeader(header);

    // first page
    // show current used oAuth key
    popup.addPanel("", dvcs.connector.plugin.soy.repositoryOAuthDialog({
        'organizationId': org.id,
        'oAuthKey': org.credential.key,
        'isOnDemandLicense': dvcs.connector.plugin.onDemandLicense,
        'isIntegratedAccount': org.integratedAccount,
        'edit': false,
        'aciEnabled': dvcs.connector.plugin.aciEnabled
    }));

    popup.addButton(AJS.I18n.getText("common.forms.edit"), function (dialog) {
        popup.nextPage();
        popup.updateHeight();
    }, "edit-button");

    addCancelButtonToPopup();

    // add 2nd page
    popup.addPage();

    popup.addHeader(header);
    // show form to set new oAuth key & secret
    popup.addPanel("", dvcs.connector.plugin.soy.repositoryOAuthDialog({
        'organizationId': org.id,
        'oAuthKey': org.credential.key,
        'isOnDemandLicense': dvcs.connector.plugin.onDemandLicense,
        'isIntegratedAccount': org.integratedAccount,
        'edit': true,
        'aciEnabled': dvcs.connector.plugin.aciEnabled
    }));


    _clearFieldError(AJS.$("#updateOAuthForm #key"));
    _clearFieldError(AJS.$("#updateOAuthForm #secret"));

    // button to return back to 1st page
    popup.addButton(AJS.I18n.getText("common.concepts.back"), function (dialog) {
        popup.prevPage();
        popup.updateHeight();
    });

    // button to submit new oAuth credentials
    popup.addButton(submitButtonText, function (dialog) {
        // validate
        var v1 = _validateField(AJS.$("#updateOAuthForm #key"), "OAuth key must not be blank");
        var v2 = _validateField(AJS.$("#updateOAuthForm #secret"), "OAuth secret must not be blank");
        popup.updateHeight();
        if (!v1 || !v2) {
            return;
        }

        AJS.$("#repositoryOAuthDialog .dialog-button-panel button").attr("disabled", "disabled");
        AJS.$("#repositoryOAuthDialog .dialog-button-panel button").attr("aria-disabled", "true");
        AJS.$("#repositoryOAuthDialog .dialog-button-panel").prepend(
            "<span class='aui-icon aui-icon-wait' style='padding-right:10px'>Wait</span>");

        // submit form
        AJS.$.post(BASE_URL + "/rest/bitbucket/1.0/org/" + org.id + "/oauth", AJS.$("#updateOAuthForm").serialize())
            .done(function (data) {
                if (success) {
                    success(popup);
                }
            })
            .error(function (err) {
                _showOAuthErrorMsg(err);
                popup.updateHeight();
            });
    }, "aui-button submit");

    addCancelButtonToPopup();

    // start popup
    popup.gotoPage(0);
    popup.show();
    popup.updateHeight();

    return false;
}

/**
 * Show the configure oauth settings dialog for a user-added account
 * @param org The org to configure
 * @param atlToken The csrf token to append to page redirects
 */
function configureOAuthForUserAccount(org, atlToken) {

    showOAuthDialog(org, "repositoryOAuthDialog", "Configure OAuth for account " + org.name, "Regenerate Access Token", function (popup) {
        var actionName;
        if (org.dvcsType == "bitbucket") {
            actionName = "RegenerateBitbucketOauthToken.jspa";
        }
        else if (org.dvcsType == "github") {
            actionName = "RegenerateGithubOauthToken.jspa";
        }
        else {
            actionName = "RegenerateGithubEnterpriseOauthToken.jspa";
        }

        window.location.replace(BASE_URL + "/secure/admin/" + actionName + "?organization=" + org.id + "&atl_token=" + atlToken);
    });

    AJS.$.getJSON(BASE_URL + "/rest/bitbucket/1.0/organization/" + org.id + "/tokenOwner", function (data) {
        if (data.fullName.trim().length == 0) {
            data.fullName = data.username;
        }
        AJS.$(".repositoryOAuthDialog #tokenUser").html(dvcs.connector.plugin.soy.repositoryOAuthDialogTokenOwner(data));
    }).error(function (err) {
        AJS.$(".repositoryOAuthDialog #tokenUser").html("<i>&lt;Invalid, please regenerate access token.&gt;<i>");
    });

    return false;
}

/**
 * Show the configure OAuth settings for an integrated account
 *
 * @param org The org to configure
 */
function configureOAuthForIntegratedAccount(org) {

    showOAuthDialog(org, "OAuthSettingsDialog", "OAuth settings for account " + org.name, "Save", function (popup) {
        popup.hide();
        ConfigureOrganization.syncRepositoryList(org.id, org.name);
    });

    return false;
}

function registerDropdownCheckboxHandlers() {
    AJS.$("a[id^='org_autolink_check']").on({
        "aui-dropdown2-item-check": function () {
            autoLinkIssuesOrg(this.id.substring("org_autolink_check".length), this.id, true);
        },
        "aui-dropdown2-item-uncheck": function () {
            autoLinkIssuesOrg(this.id.substring("org_autolink_check".length), this.id, false);
        }
    });

    AJS.$("a[id^='org_global_smarts']").on({
        "aui-dropdown2-item-check": function () {
            enableSmartcommitsOnNewRepos(this.id.substring("org_global_smarts".length), this.id, true);
        },
        "aui-dropdown2-item-uncheck": function () {
            enableSmartcommitsOnNewRepos(this.id.substring("org_global_smarts".length), this.id, false);
        }
    });
}

function autoLinkIssuesOrg(organizationId, checkboxId, checkedValue) {
    var $orgAutoLinkCheckbox = AJS.$("#" + checkboxId);
    $orgAutoLinkCheckbox.addClass("disabled");
    var restClient = new DvcsConnectorRestClient(BASE_URL);
    restClient.organization
        .autoLinkRepositories(organizationId, checkedValue)
        .done(function (data) {
            var $orgSmartCommitsCheckbox = AJS.$("#org_global_smarts" + organizationId);
            $orgAutoLinkCheckbox.removeClass("disabled");
            if (!checkedValue && $orgSmartCommitsCheckbox) {
                $orgSmartCommitsCheckbox.addClass("disabled");
            } else {
                $orgSmartCommitsCheckbox.removeClass("disabled");
            }
        })
        .fail(function (err) {
            showError("Unexpected error occurred. Please contact the server administrator.", "#aui-message-bar-" + organizationId);
            $orgAutoLinkCheckbox.removeClass("disabled");
            setCheckedDropdown2(checkboxId, !checkedValue);
        });
}

function enableSmartcommitsOnNewRepos(organizationId, checkboxId, checkedValue) {
    var $orgAutoSmartCommitsCheckbox = AJS.$("#" + checkboxId);
    $orgAutoSmartCommitsCheckbox.addClass("disabled");
    var restClient = new DvcsConnectorRestClient(BASE_URL);
    restClient.organization
        .autoEnableSmartCommits(organizationId, checkedValue)
        .done(function (data) {
            $orgAutoSmartCommitsCheckbox.removeClass("disabled");
        })
        .fail(function (err) {
            showError("Unexpected error occurred when enabling smart commits on new repositories. Please contact the server administrator.", "#aui-message-bar-" + organizationId);
            $orgAutoSmartCommitsCheckbox.removeClass("disabled");
            setCheckedDropdown2(checkboxId, !checkedValue);
        });
}

function registerAdminPermissionInlineDialogTooltips() {
    AJS.$(".admin-permission").each(function (index) {
        registerAdminPermissionInlineDialogTooltip(this);
    });
}

function enableRepoSmartcommits(repoId, checkboxId) {
    var checkedValue = AJS.$("#" + checkboxId).is(":checked");
    AJS.$("#" + checkboxId).attr("disabled", "disabled");
    AJS.$("#" + checkboxId + "working").show();
    AJS.$.ajax(
        {
            type: 'POST',
            dataType: "json",
            contentType: "application/json",
            url: BASE_URL + "/rest/bitbucket/1.0/repo/" + repoId + "/smart",
            data: '{ "payload" : "' + checkedValue + '"}',

            success: function (data) {
                AJS.$("#" + checkboxId + "working").hide();
                AJS.$("#" + checkboxId).removeAttr("disabled");
            }
        }
    ).error(function (err) {
        showError("Unexpected error occurred.");
        AJS.$("#" + checkboxId + "working").hide();
        AJS.$("#" + checkboxId).removeAttr("disabled");
        setChecked(checkboxId, !checkedValue);
    });
}

function confirmationDialog(options) {
    var dialog = new AJS.Dialog({width: 500, height: 150, id: "confirm-dialog", closeOnOutsideClick: false});
    dialog.addHeader(options.header);
    dialog.addPanel("ConfirmPanel", options.body + "<div id='aui-message-bar-confirmation-dialog'></div>");

    dialog.addButtonPanel();
    dialog.page[0].buttonpanel.append("<span id='confirm-action-wait' class='aui-icon' style='padding-right:10px'>&nbsp;</span>");

    dialog.addSubmit(options.submitButtonLabel, function (dialog, event) {
        dialog.working(true);
        if (typeof options.okAction == 'function') {
            options.okAction(dialog);
        }
    });

    dialog.addCancel("Cancel", function (dialog) {
        if (typeof options.cancelAction == 'function') {
            options.cancelAction(dialog);
        }
        dialog.remove();
    }, "#");

    dialog.disableActions = function () {
        AJS.$('#confirm-dialog .button-panel-submit-button').attr("disabled", "disabled");
        AJS.$('#confirm-dialog .button-panel-submit-button').attr("aria-disabled", "true");
        AJS.$('#confirm-dialog .button-panel-cancel-link').addClass('dvcs-link-disabled');
    }

    dialog.enableActions = function () {
        AJS.$('#confirm-dialog .button-panel-submit-button').removeAttr("disabled");
        AJS.$('#confirm-dialog .button-panel-submit-button').removeAttr("aria-disabled");
        AJS.$('#confirm-dialog .button-panel-cancel-link').removeClass('dvcs-link-disabled');
    }

    dialog.working = function (working) {
        if (working) {
            AJS.$("#confirm-action-wait").addClass("aui-icon-wait");
            this.disableActions();
        } else {
            AJS.$("#confirm-action-wait").removeClass("aui-icon-wait");
            this.enableActions();
        }
    }

    dialog.showError = function (message) {
        dialog.working(false);
        showError(message, "#aui-message-bar-confirmation-dialog");
        dialog.updateHeight();
    }

    dialog.isAttached = function () {
        return !AJS.$.isEmptyObject(dialog.popup.element);
    }

    dialog.show();
    dialog.updateHeight();

    return dialog;
}

function showError(message, auiMessageElement, closeable) {
    if (typeof auiMessageElement == 'undefined') {
        auiMessageElement = "#aui-message-bar-global";
    }

    AJS.$(auiMessageElement).empty();
    AJS.messages.error(auiMessageElement, {
        title: message,
        closeable: closeable
    });
}

function setChecked(checkboxId, checked) {
    if (checked) {
        AJS.$("#" + checkboxId).attr("checked", "checked");
    } else {
        AJS.$("#" + checkboxId).removeAttr("checked");
    }
}

function setCheckedDropdown2(checkboxId, checked) {
    if (checked) {
        AJS.$("#" + checkboxId).addClass("checked");
    } else {
        AJS.$("#" + checkboxId).removeClass("checked");
    }
}

function dvcsShowHidePanel(id) {
    var jqElement = AJS.$("#" + id);
    if (jqElement.is(":visible")) {
        AJS.$("#" + id).fadeOut().slideUp();
    } else {
        AJS.$("#" + id).fadeIn().slideDown();
    }
}
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

function parseAccountUrl(url) {
    var pattern = /(.*)\/(.+?)\/?$/;
    var matches = url.match(pattern);
    if (matches)
        return {hostUrl: matches[1], name: matches[2]};
}

function getSourceDiv() {
    return AJS.$("#dvcs-connect-source");
}

function triggerAnalyticsEvent(eventType) {
    if (AJS.EventQueue) {
        AJS.EventQueue.push({
            name: "jira.dvcsconnector.config." + eventType,
            properties: {source: getSourceDiv().data("sourceOrDefault")}
        });
    }
}

//------------------------------------------------------------

AJS.$(function () {
    if (typeof init_repositories == 'function') {
        // cancel annoying leave message even when browser pre-fill some fields
        window.onbeforeunload = function () {
        };

        // defined in macro
        init_repositories();

        /**
         * DVCS connector uses the hash '#expand' in the URL to determine whether to automatically open the
         * 'Add New Account' dialog.
         */
        if (window.location.hash == '#expand') {
            var hostToSelect = undefined;
            if (parseUri) {
                //queryKey should always be available in the object returned by parseUri(), but it's good to be
                // defensive anyway
                var urlQueries = parseUri(window.location.href).queryKey || {};
                hostToSelect = urlQueries.selectHost;
            }
            showAddRepoDetails(true, hostToSelect);
        }
    }
});

function dvcsLogConsole(msg) {
    if (window.console && window.console.log) {
        console.log("DVCS: " + msg);
    }
}
//---------------------------------------------------------

AJS.$.fn.extend({
    dvcsGearMenu: function (opts) {
        // original AUI dropdown
        this.dropDown();
        // stop further propagation - causes not hide dropdown menu
        AJS.$(opts.noHideItemsSelector).bind("click", function (e) {
            e.stopPropagation();
        });
    }
});
