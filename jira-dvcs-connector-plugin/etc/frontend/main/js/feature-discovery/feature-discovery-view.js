/**
 * The view that shows the DVCS Connector feature discovery wizard
 *
 * @module jira-dvcs-connector/feature-discovery/feature-discovery-view
 */
define('jira-dvcs-connector/feature-discovery/feature-discovery-view', [
    'jira-dvcs-connector/lib/backbone',
    'jira-dvcs-connector/aui/dialog2'
], function(
    Backbone,
    dialog2
) {
    'use strict';

    var FEATURE_DISCOVERY_PAGES = [
        {
            pageName: "clearer-picture",
            headingText: AJS.I18n.getText("com.atlassian.jira.plugins.dvcs.featurediscovery.clearer-picture.heading"),
            bodyText: AJS.I18n.getText("com.atlassian.jira.plugins.dvcs.featurediscovery.clearer-picture.text"),
            imageClass: "dvcs-feature-discovery-clearer-picture-img"
        },
        {
            pageName: "do-once",
            headingText: AJS.I18n.getText("com.atlassian.jira.plugins.dvcs.featurediscovery.do-once.heading"),
            bodyText: AJS.I18n.getText("com.atlassian.jira.plugins.dvcs.featurediscovery.do-once.text"),
            imageClass: "dvcs-feature-discovery-do-once-img"
        },
        {
            pageName: "release-with-confidence",
            headingText: AJS.I18n.getText("com.atlassian.jira.plugins.dvcs.featurediscovery.release.heading"),
            bodyText: AJS.I18n.getText("com.atlassian.jira.plugins.dvcs.featurediscovery.release.text"),
            imageClass: "dvcs-feature-discovery-release-img"
        }
    ];

    /**
     * The Feature Discovery dialog view
     *
     * Events:
     * - tourFinished: Emitted when a user reaches the end of the tour and clicks the action button
     * - tourAborted: Emitted when a user closes the tour without reaching the end (e.g. ESC, or close)
     * - pageChanged: Emitted when the tour page changes
     * - close: Emitted when the tour is closed (after the tourFinished/tourAborted events)
     */
    return Backbone.View.extend({

        events: {
            "aui-tour-finish aui-tour": "_triggerTourFinished",
            "aui-tour-page-change aui-tour": "_handleTourPageChange",
            "click .aui-dialog2-header-close": "_triggerTourAborted",
            "click .aui-tour-next": "_handlePageChangeClick",
            "click .aui-tour-prev": "_handlePageChangeClick",
            "click .aui-tour-page-indicator a": "_handlePageChangeClick"
        },

        template: JIRA.Templates.DVCSConnector.FeatureDiscovery.dialog,

        initialize: function() {
            this.dialog = dialog2(this.template({pages: FEATURE_DISCOVERY_PAGES}));

            this.$el = this.dialog.$el;

            // In some browsers a page change event is fired on show
            // We want to track if a user has actually changed the page in order to do the correct
            // show/hide of link elements
            this._userHasTriggeredActualPageChange = false;

            // This relies on the fact that tour completion triggers a 'remove' event on the dialog rather than a 'hide'
            this.dialog.on("hide", this._triggerTourAborted.bind(this));
        },

        /**
         * Show the feature discovery dialog.
         *
         * @returns {FeatureDiscovery} the Feature Discovery instance
         */
        show: function() {
            this.dialog.show();
            this._hideNonVisibleTabbableElements();
            this._setFocusOnNext();
            return this;
        },

        /**
         * Closes the feature discovery dialog and cleans up resources.
         *
         * Once closed do not interact with the feature discovery instance
         * as it will be in an invalid state.
         */
        close: function() {
            this.remove();
            this.dialog.remove();
            this.trigger("closed");
        },

        /**
         * Get the current page the tour is on
         *
         * @returns {String} The name of the current page the tour is on
         */
        getCurrentPageName: function() {
            return FEATURE_DISCOVERY_PAGES[this.getCurrentPageIndex()].pageName;
        },

        /**
         * Get the (0-indexed) index of the current page the tour is on
         *
         * @returns {number} The index of the current page the tour is on
         */
        getCurrentPageIndex: function() {
            return Number(this.$el.find('aui-tour').attr('page'));
        },

        _handleTourPageChange: function() {
            if (!this._userHasTriggeredActualPageChange) {
                this._hideNonVisibleTabbableElements();
                return;
            }

            // This is a workaround for a bug where tabbable elements on tour pages can be tabbed to,
            // which breaks the tour appearance.
            // Sets all links to hidden once the transition animation has completed
            var self = this;
            this._showTabbableElements();
            this.$el
                .find("div.aui-tour-content")
                .one('webkitTransitionEnd otransitionend oTransitionEnd transitionend', function() {
                    self._hideNonVisibleTabbableElements();
                });

            this.trigger("pageChanged");
        },

        _handlePageChangeClick: function() {
            // The first time a user actually triggers a page change,
            // run the tour page changed logic (which will have been skipped to
            // account for some browsers that fire a page change event on show)
            if (!this._userHasTriggeredActualPageChange) {
                this._userHasTriggeredActualPageChange = true;
                this._handleTourPageChange();
            }
        },

        _triggerTourFinished: function() {
            this.trigger("tourFinished");
            this.close();
        },

        _triggerTourAborted: function() {
            this.trigger("tourAborted");
            this.close();
        },

        _showTabbableElements: function() {
            this.$el
                .find('div.aui-tour-page div.dvcs-feature-discovery-link-container')
                .removeClass('hidden');
            this.trigger("_pageLinkElementsShown");
        },

        _hideNonVisibleTabbableElements: function() {
            this.$el
                .find('div.aui-tour-page[aria-hidden="true"] div.dvcs-feature-discovery-link-container')
                .addClass('hidden');
            this.trigger("_pageLinkElementsHidden");
        },

        _setFocusOnNext: function() {
            var $nextButton = this.$el.find('.aui-tour-next');
            if ($nextButton.length == 0) {
                setTimeout(this._setFocusOnNext.bind(this), 100);
            }
            $nextButton.focus();
        }

    })

});