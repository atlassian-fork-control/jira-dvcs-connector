/**
 * The 'connection successful' dialog shown on successful approval of a Bitbucket organization connected
 * with easy connect.
 *
 * @module jira-dvcs-connector/bitbucket/views/connection-successful
 */
define('jira-dvcs-connector/bitbucket/views/connection-successful', ['require'], function (require) {
    "use strict";

    var dialog2 = require('jira-dvcs-connector/aui/dialog2');
    var ConfigDialog = require('jira-dvcs-connector/bitbucket/views/repo-config-dialog');
    var AnalyticsClient = require('jira-dvcs-connector/analytics/analytics-client');
    var analyticsClient = new AnalyticsClient();

    /**
     * A view that will render the Connection Successful dialog into the page and show it.
     */
    return ConfigDialog.extend({

        template: JIRA.Templates.DVCSConnector.Bitbucket.connectionSuccessfulDialog,
        events: {
            'click #dialog-submit-button': '_submit'
        },

        /**
         * Initialize the view. Invoked on construction of a new view instance.
         *
         * @param {Object} options - Initialization options
         * @param {int} options.organizationId - The ID of the organization the dialog is for
         * @param {string} options.organizationName - The organization name to display on the dialog
         * @param {string} options.baseUrl - The JIRA base URL to use when updating the organization
         * @param {function} options.onSuccess - A callback to invoke on successful update of
         *  autolink/smartcommits settings
         * @param {function} options.onError - A callback to invoke on failed update of autolink/smartcommits settings
         */
        initialize: function (options) {
            this.orgId = options.organizationId;
            this.onSuccess = options.onSuccess;
            this.onError = options.onError;

            this.dialog = dialog2(this.template({
                organizationName: options.organizationName
            }));
            this._initialize();
        },

        _submit: function () {
            this.$submitSpinner.toggleClass('hidden');
            this.$submitButton.attr('disabled', 'disabled');
            this.$autolinkCheckbox.attr('disabled', 'disabled');
            this.$smartCommitsCheckbox.attr('disabled', 'disabled');

            this.submitUpdates(analyticsClient.fireConfigCompleted);
        }
    });

});