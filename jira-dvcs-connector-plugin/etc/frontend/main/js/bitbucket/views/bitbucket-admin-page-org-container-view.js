/**
 * Listens for events that are related to the org container
 *
 * @module jira-dvcs-connector/bitbucket/views/bitbucket-admin-page-org-container-view
 */
define('jira-dvcs-connector/bitbucket/views/bitbucket-admin-page-org-container-view', ['require'], function (require) {
    "use strict";

    var Backbone = require('jira-dvcs-connector/lib/backbone');
    var PendingOrgView = require('jira-dvcs-connector/bitbucket/views/bitbucket-admin-page-pending-org-view');

    return Backbone.View.extend({

        initialize: function () {
            this.orgId = this.$el.data('org-id');
            this.pendingOrgSection = new PendingOrgView({
                el: this.$('.pending-bb-org')
            });
            this.pendingOrgSection.on('jira-dvcs-connector:pending-org-deleted',
                    this._pendingOrgDeleted, this);
        },

        _pendingOrgDeleted: function () {
            this.trigger('jira-dvcs-connector:pending-org-deleted');
        }
    });
});
