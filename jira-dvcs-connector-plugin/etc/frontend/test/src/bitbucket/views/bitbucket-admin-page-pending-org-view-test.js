define([
    'fusion/test/qunit',
    'fusion/test/hamjest',
    'jquery',
    'fusion/test/mock-declaration',
    'fusion/test/backbone'
], function (QUnit,
             __,
             $,
             MockDeclaration,
             Backbone
){

    const pendingOrgViewSnippet = `
        <div data-bitbucket-override-url="http://dev.bitbucket.org:8000" data-org-name="test" data-org-id="1">
            <section class="approve-org-section" role="main">
                <section class="bb-org-approval-details">
                    <h2 class="bb-org-access-request">Your test account is requesting access to JIRA</h2>
                    <div>
                        <div>The <strong>test</strong> account on Bitbucket is requesting the ability to:</div>
                        <div class="scopes">
                            <div class="scopeItem">
                                <div class="aui-group aui-group-split">
                                    <div class="aui-item scope-icon"><span class="aui-icon icon-issues"></span></div>
                                    <div class="aui-item scope-text">View and update your issues</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form class="buttons-container no-left-padding buttons aui">
                        <button class="aui-button aui-button-primary approve-org-button">Grant access</button>
                        <button class="aui-button aui-button-link remove-connection-button">Remove connection</button>
                        <span class="confirmation-spinner aui-icon aui-icon-wait hidden"></span></form>
                </section>
            </section>
        </div>
    `;

    QUnit.module('jira-dvcs-connector/bitbucket/views/bitbucket-admin-page-pending-org-view', {
        require: {
            main: 'jira-dvcs-connector/bitbucket/views/bitbucket-admin-page-pending-org-view'
        },
        mocks: {
            backbone: QUnit.moduleMock('jira-dvcs-connector/lib/backbone', function() {
                return Backbone;
            }),
            restClient: QUnit.moduleMock('jira-dvcs-connector/rest/dvcs-connector-rest-client',
                new MockDeclaration([
                    'organization.remove'
                ]).withConstructor()
            ),
            contextPath: QUnit.moduleMock('wrm/context-path', function () {
                return function() {
                    return 'context-path'
                };
            }),
            configureOrganization: QUnit.moduleMock('jira-dvcs-connector/admin/configure-organization',
                new MockDeclaration([])
            ),
            analyticsClient: QUnit.moduleMock('jira-dvcs-connector/analytics/analytics-client',
                new MockDeclaration([
                    'firePendingOrgRemoved'
                ]).withConstructor()
            )
        },
        templates: {
        },
        beforeEach: function(assert, PendingOrgView) {
            this.view = new PendingOrgView({
                el: $(pendingOrgViewSnippet)
            });
        }
    });

    QUnit.test('Remove connection fires analytics and triggers event on successful removal',
        function (assert) {
            _runOrgRemovalTest(this, assert);
        }
    );

    QUnit.test('Remove connection fires analytics and triggers event on 404',
        function (assert) {
            _runOrgRemovalTest(this, assert, 404);
        }
    );

    QUnit.test('Remove connection fires analytics and triggers event on timeout',
        function (assert) {
            _runOrgRemovalTest(this, assert, 0);
        }
    );

    QUnit.test('Remove connection fires analytics and doesn\'t trigger event on other error',
        function (assert) {
            _runOrgRemovalTest(this, assert, 500, false);
        }
    );

    /**
     * Run an org removal test.
     *
     * This test will:
     * - click the "remove connection" button
     * - respond to the 'remove' REST call with success or fail
     * - assert on the expected behavior
     *
     * @param self A reference to the test
     * @param assert The assertion library
     * @param [failStatus] if provided, will respond to the 'remove' REST call with this failure status;
     *  otherwise will simulate a successful request (HTTP.200 OK).
     * @param [expectEventTriggered] whether or not the 'pending-org-deleted' event is expected to fire.
     * @private
     */
    function _runOrgRemovalTest(self, assert, failStatus, expectEventTriggered=true) {
        // setup
        const removeOrgTask = new $.Deferred();
        self.mocks.restClient.answer.organization.remove.returns(removeOrgTask);

        var eventFired = false;
        self.view.on('jira-dvcs-connector:pending-org-deleted', function() {
            eventFired = true;
        });

        // execute
        self.view.$el.find('.remove-connection-button').click();
        if (failStatus) {
            removeOrgTask.reject({
                status: failStatus
            });
        }
        else {
            removeOrgTask.resolve();
        }

        // verify
        assert.assertThat("Expected the org deleted event to fire", eventFired, __.is(expectEventTriggered));

        assert.assertThat("Expected the org remove to happen",
            self.mocks.restClient.answer.organization.remove, __.calledOnce());

        assert.assertThat("Expected the pending org removed analytics to fire",
            self.mocks.analyticsClient.answer.firePendingOrgRemoved, __.calledOnce());
    }
});