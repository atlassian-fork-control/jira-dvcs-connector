package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.auth;

import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.BaseRemoteRequestor;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.HttpClientProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.RemoteRequestor;

/**
 * NoAuthAuthProvider
 * <p>
 * Created on 13.7.2012, 17:17:14
 *
 * @author jhocman@atlassian.com
 */
public class NoAuthAuthProvider extends AbstractAuthProvider {
    public NoAuthAuthProvider(String hostUrl, HttpClientProvider httpClientProvider) {
        super(hostUrl, httpClientProvider);
    }

    @Override
    public RemoteRequestor provideRequestor() {
        return new BaseRemoteRequestor(this, httpClientProvider);
    }
}

