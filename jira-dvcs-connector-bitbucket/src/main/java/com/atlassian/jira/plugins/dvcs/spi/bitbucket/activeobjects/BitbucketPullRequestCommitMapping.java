package com.atlassian.jira.plugins.dvcs.spi.bitbucket.activeobjects;

import net.java.ao.Entity;
import net.java.ao.schema.Table;

@Table("BITBUCKET_PR_COMMITS")
public interface BitbucketPullRequestCommitMapping extends Entity {
    String LOCAL_ID = "LOCAL_ID";
    String PULL_REQUEST_ID = "PULL_REQUEST_ID";
    String NODE = "NODE";
    String NEXT_NODE = "NEXT_NODE";

    int getLocalId();

    void setLocalId(int id);

    int getPullRequestId();

    void setPullRequestId(int pullRequestId);

    String getNode();

    void setNode(String node);

    String getNextNode();

    void setNextNode(String nextNode);
}
