package com.atlassian.jira.plugins.dvcs.spi.bitbucket;

import com.atlassian.jira.plugins.dvcs.exception.SourceControlException;
import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFileAction;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFileDetail;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFileDetailsEnvelope;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.service.message.MessagingService;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicator;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicatorProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.BitbucketRemoteClient;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.JsonParsingException;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketBranchesAndTags;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketChangeset;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketChangesetFile;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketChangesetWithDiffstat;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketDiffstat;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketService;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketServiceEnvelope;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketServiceField;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints.BranchesAndTagsRemoteRestpoint;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints.ChangesetRemoteRestpoint;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints.ServiceRemoteRestpoint;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.linker.BitbucketLinker;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.sal.api.ApplicationProperties;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class BitbucketCommunicatorTest {
    private static final String REPO_SLUG = "SLUG";
    private static final String ORG_NAME = "ORG";
    private static final String CHANGESET_NODE = "some-hash";

    @Mock
    private Repository repositoryMock;
    @Mock
    private BitbucketLinker bitbucketLinkerMock;
    @Mock
    private PluginAccessor pluginAccessorMock;
    @Mock
    private BitbucketClientBuilderFactory bitbucketClientBuilderFactoryMock;
    @Mock
    private MessagingService messagingServiceMock;
    @Mock
    private BitbucketRemoteClient bitbucketRemoteClientMock;
    @Mock
    private BranchesAndTagsRemoteRestpoint branchesAndTagsRemoteRestMock;
    @Mock
    private BitbucketBranchesAndTags bitbucketBranchesAndTagsMock;
    @Mock
    private ChangesetRemoteRestpoint changesetRestpoint;
    @Mock
    private Plugin pluginMock;
    @Mock
    private PluginInformation pluginInformationMock;
    @Mock
    private ServiceRemoteRestpoint servicesRestMock;
    @Mock
    private ApplicationProperties applicationPropertiesMock;
    @Mock
    private DvcsCommunicatorProvider dvcsCommunicatorProvider;
    @Mock
    private Changeset changeset;

    private DvcsCommunicator communicator;

    @BeforeMethod
    public void setup() {
        initMocks(this);

        when(pluginInformationMock.getVersion()).thenReturn("0");
        when(pluginMock.getPluginInformation()).thenReturn(pluginInformationMock);
        when(pluginAccessorMock.getPlugin(anyString())).thenReturn(pluginMock);

        final BitbucketClientBuilder bitbucketClientBuilderMock = mock(BitbucketClientBuilder.class, new BuilderAnswer());
        when(bitbucketClientBuilderFactoryMock.forRepository(any(Repository.class))).thenReturn(bitbucketClientBuilderMock);
        communicator = new BitbucketCommunicator(bitbucketLinkerMock, pluginAccessorMock, bitbucketClientBuilderFactoryMock, applicationPropertiesMock);

        when(bitbucketClientBuilderMock.build()).thenReturn(bitbucketRemoteClientMock);
        when(bitbucketRemoteClientMock.getChangesetsRest()).thenReturn(changesetRestpoint);
        when(bitbucketRemoteClientMock.getBranchesAndTagsRemoteRestpoint()).thenReturn(branchesAndTagsRemoteRestMock);

        when(repositoryMock.getSlug()).thenReturn(REPO_SLUG);
        when(repositoryMock.getOrgName()).thenReturn(ORG_NAME);
        when(repositoryMock.getOrgHostUrl()).thenReturn("https://some/url");

        when(servicesRestMock.getAllServices(ORG_NAME, REPO_SLUG)).thenReturn(sampleServices());
        when(bitbucketRemoteClientMock.getServicesRest()).thenReturn(servicesRestMock);

        when(applicationPropertiesMock.getBaseUrl()).thenReturn("http://jira.example.com");
        when(changeset.getNode()).thenReturn(CHANGESET_NODE);
    }

    @Test
    public void testSetupPostHookShouldDeleteOrphan() {
        String hookUrl = "http://jira.example.com" + DvcsCommunicator.POST_HOOK_SUFFIX + "5/sync";
        communicator.ensureHookPresent(repositoryMock, hookUrl);

        verify(servicesRestMock, times(1)).addPOSTService(ORG_NAME, REPO_SLUG, hookUrl);
        verify(servicesRestMock, times(1)).deleteService(ORG_NAME, REPO_SLUG, 111);
        verify(servicesRestMock, times(1)).deleteService(ORG_NAME, REPO_SLUG, 101);
    }

    @Test
    public void testSetupPostHookAlreadySetUpShouldDeleteOrphan() {
        List<BitbucketServiceEnvelope> sampleServices = sampleServices();
        sampleServices.add(sampleService("http://jira.example.com/rest/bitbucket/1.0/repository/5/sync", 1));
        when(servicesRestMock.getAllServices(ORG_NAME, REPO_SLUG)).thenReturn(sampleServices);

        String hookUrl = "http://jira.example.com" + DvcsCommunicator.POST_HOOK_SUFFIX + "5/sync";
        communicator.ensureHookPresent(repositoryMock, hookUrl);

        verify(servicesRestMock, never()).addPOSTService(ORG_NAME, REPO_SLUG, hookUrl);
        verify(servicesRestMock, times(1)).deleteService(ORG_NAME, REPO_SLUG, 111);
        verify(servicesRestMock, times(1)).deleteService(ORG_NAME, REPO_SLUG, 101);
    }

    @Test
    public void testFileDetails() throws Exception {
        BitbucketChangesetWithDiffstat diffstat = new BitbucketChangesetWithDiffstat();
        diffstat.setType("MODIFIED");
        diffstat.setFile("my_file");
        BitbucketDiffstat bitbucketDiffstat = new BitbucketDiffstat();
        bitbucketDiffstat.setAdded(1);
        bitbucketDiffstat.setRemoved(2);
        diffstat.setDiffstat(bitbucketDiffstat);

        when(changesetRestpoint.getChangesetDiffStat(eq(ORG_NAME), eq(REPO_SLUG), eq(CHANGESET_NODE), anyInt())).thenReturn(Collections.singletonList(diffstat));

        ChangesetFileDetailsEnvelope changesetFileDetailsEnvelope = communicator.getFileDetails(repositoryMock, changeset);

        List<ChangesetFileDetail> fileDetails = changesetFileDetailsEnvelope.getFileDetails();
        assertThat(fileDetails, is(ImmutableList.of(
                new ChangesetFileDetail(ChangesetFileAction.MODIFIED, diffstat.getFile(), diffstat.getDiffstat().getAdded(), diffstat.getDiffstat().getRemoved()))
        ));
        assertThat(changesetFileDetailsEnvelope.getCount(), is(1));
        verify(changesetRestpoint).getChangesetDiffStat(anyString(), anyString(), anyString(), anyInt());
        verify(changesetRestpoint, never()).getChangeset(anyString(), anyString(), anyString());
    }

    @Test
    public void testFileDetailsMoreFiles() throws Exception {
        List<BitbucketChangesetWithDiffstat> diffstats = new LinkedList<>();
        List<BitbucketChangesetFile> bitbucketChangesetFiles = new LinkedList<>();

        int fileCount = Changeset.MAX_VISIBLE_FILES + 1;
        for (int i = 0; i < fileCount; i++) {
            if (i < Changeset.MAX_VISIBLE_FILES) {
                BitbucketChangesetWithDiffstat diffstat = new BitbucketChangesetWithDiffstat();
                diffstat.setType("MODIFIED");
                diffstat.setFile("my_file_" + i);
                BitbucketDiffstat bitbucketDiffstat = new BitbucketDiffstat();
                bitbucketDiffstat.setAdded(1 + i);
                bitbucketDiffstat.setRemoved(2 + i);
                diffstat.setDiffstat(bitbucketDiffstat);
                diffstats.add(diffstat);
            }

            BitbucketChangesetFile bitbucketChangesetFile = new BitbucketChangesetFile();
            bitbucketChangesetFile.setType("MODIFIED");
            bitbucketChangesetFile.setFile("my_file1_" + i);
            bitbucketChangesetFiles.add(bitbucketChangesetFile);
        }

        BitbucketChangeset bitbucketChangeset = new BitbucketChangeset();
        bitbucketChangeset.setNode(changeset.getNode());
        bitbucketChangeset.setFiles(bitbucketChangesetFiles);

        when(changesetRestpoint.getChangesetDiffStat(eq(ORG_NAME), eq(REPO_SLUG), eq(CHANGESET_NODE), anyInt())).thenReturn(diffstats);
        when(changesetRestpoint.getChangeset(eq(ORG_NAME), eq(REPO_SLUG), eq(CHANGESET_NODE))).thenReturn(bitbucketChangeset);

        ChangesetFileDetailsEnvelope changesetFileDetailsEnvelope = communicator.getFileDetails(repositoryMock, changeset);

        List<ChangesetFileDetail> fileDetails = changesetFileDetailsEnvelope.getFileDetails();
        int i = 0;
        for (ChangesetFileDetail changesetFileDetail : fileDetails) {
            assertThat(changesetFileDetail, is(new ChangesetFileDetail(ChangesetFileAction.MODIFIED, "my_file_" + i, 1 + i, 2 + i)));
            i++;
        }
        assertThat(changesetFileDetailsEnvelope.getFileDetails().size(), is(Changeset.MAX_VISIBLE_FILES));
        assertThat(changesetFileDetailsEnvelope.getCount(), is(fileCount));
        verify(changesetRestpoint).getChangesetDiffStat(anyString(), anyString(), anyString(), anyInt());
        verify(changesetRestpoint).getChangeset(anyString(), anyString(), anyString());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testNullRepositoryToCreatePullRequestUrl() {
        communicator.getCreatePullRequestUrl(null, "hello");
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testNullSourceBranchToCreatePullRequestUrl() {
        communicator.getCreatePullRequestUrl(repositoryMock, null);
    }

    @Test
    public void getCommitUrl_isCorrect_whenNonNullsProvided() {
        final String url = communicator.getCommitUrl(repositoryMock, changeset);
        assertThat(url, is("https://some/url/ORG/SLUG/changeset/some-hash?dvcsconnector=0"));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void getCommitUrl_fails_whenNullRepositoryProvided() {
        communicator.getCommitUrl(null, changeset);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void getCommitUrl_fails_whenNullChangesetProvided() {
        communicator.getCommitUrl(repositoryMock, null);
    }

    @Test
    public void getFileCommitUrl_isCorrect_whenNonNullsProvided() {
        final String url = communicator.getFileCommitUrl(repositoryMock, changeset, "file", 123);
        assertThat(url, is("https://some/url/ORG/SLUG/changeset/some-hash?dvcsconnector=0#chg-file"));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void getFileCommitUrl_fails_whenNullRepositoryProvided() {
        communicator.getFileCommitUrl(null, changeset, "file", 123);
    }

    @Test(expectedExceptions = SourceControlException.BitbucketInvalidResponseException.class)
    public void shouldThrowWhenWrongBitbucketResponseReceived() {
        when(branchesAndTagsRemoteRestMock.getBranchesAndTags(anyString(), anyString()))
                .thenThrow(new JsonParsingException(null));
        communicator.getBranches(repositoryMock);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void getFileCommitUrl_fails_whenNullChangesetProvided() {
        communicator.getFileCommitUrl(repositoryMock, null, "file", 123);
    }

    private List<BitbucketServiceEnvelope> sampleServices() {
        List<BitbucketServiceEnvelope> services = Lists.newArrayList();
        services.add(sampleService("http://jira.example.com/rest/bitbucket/1.0/repository/55/sync", 111));
        services.add(sampleService("http://jira.example.com/rest/bitbucket/1.0/repository/54/sync", 101));
        return services;
    }

    private BitbucketServiceEnvelope sampleService(String url, int id) {
        BitbucketServiceEnvelope e1 = new BitbucketServiceEnvelope();
        BitbucketService s1 = new BitbucketService();
        s1.setType(ServiceRemoteRestpoint.SERVICE_TYPE_POST);
        List<BitbucketServiceField> fields1 = Lists.newArrayList();
        BitbucketServiceField field11 = new BitbucketServiceField();
        field11.setName("URL");
        field11.setValue(url);
        fields1.add(field11);
        s1.setFields(fields1);
        e1.setService(s1);
        e1.setId(id);
        return e1;
    }

    private static class BuilderAnswer implements Answer<Object> {
        @Override
        public Object answer(InvocationOnMock invocation) throws Throwable {
            Object builderMock = invocation.getMock();
            if (invocation.getMethod().getReturnType().isInstance(builderMock)) {
                return builderMock;
            } else {
                return Mockito.RETURNS_DEFAULTS.answer(invocation);
            }
        }
    }
}
